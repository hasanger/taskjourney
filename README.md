# <img src="logo.png" alt="TaskJourney logo" width="56" height="56" />  TaskJourney

A powerful tool for managing your life. TaskJourney combines a to-do list and activity logger (TaskJourney), a program that helps you remember facts (SecondMemory), and a note-taking and journal app (TaskJournal). Work in progress.

## Setup

### 1. Download

```
git clone https://gitlab.com/hasanger/taskjourney
cd taskjourney
```

### 2. Run

#### Windows
`gradlew run`

#### Mac/Linux
`./gradlew run`


### 3. Open the web app (not working yet)
Go to `0.0.0.0:8080` in your browser.
