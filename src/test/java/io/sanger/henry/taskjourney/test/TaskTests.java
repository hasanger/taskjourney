package io.sanger.henry.taskjourney.test;

import io.sanger.henry.taskjourney.api.Database;
import io.sanger.henry.taskjourney.api.task.Task;
import io.sanger.henry.taskjourney.api.task.workspace.Workspace;
import io.sanger.henry.taskjourney.api.user.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDateTime;

import static io.sanger.henry.taskjourney.api.TaskJourneyAPI.getDatabase;
import static org.junit.jupiter.api.Assertions.*;

@ExtendWith({APILoadingExtension.class})
public class TaskTests {

    @Test
    public void testTaskCreation() throws IOException, SQLException {
        User testUser = new User("user@example.com", "Test", "Test", "test", "password");

        Task task = new Task("Test task");
        Workspace workspace = getDatabase().getWorkspace(testUser.getDefaultWorkspace());
        assertNotNull(workspace);
        workspace.addTask(task);

        Task testTask = workspace.getTask(task.getID());
        assertNotEquals(testTask, null);
        assertEquals(testTask.getName(), "Test task");
    }

}
