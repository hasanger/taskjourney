package io.sanger.henry.taskjourney.test;

import io.sanger.henry.taskjourney.api.TaskJourneyAPI;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;

import java.nio.file.Path;

public class APILoadingExtension implements BeforeAllCallback {

    private static boolean started = false;

    @Override
    public void beforeAll(ExtensionContext context) throws Exception {
        if(!started) {
            started = true;
            TaskJourneyAPI.init(Path.of("testdata"));
        }
    }

}
