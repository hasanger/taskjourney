package io.sanger.henry.taskjourney;

import io.sanger.henry.taskjourney.api.task.duerule.DueRule;
import io.sanger.henry.taskjourney.api.task.duerule.MonthlyDueRule;

import java.io.IOException;
import java.sql.SQLException;
import java.time.LocalDate;

public class Testing {

    public static void main(String[] args) throws SQLException, IOException {
        LocalDate start = LocalDate.now().minusDays(30);
        LocalDate searchStart = LocalDate.now().minusDays(32);
        LocalDate searchEnd = LocalDate.now().plusDays(35);

        DueRule testRule = new MonthlyDueRule(start, 1);
        System.out.println("Start: " + start);
        System.out.println("Search start: " + searchStart);
        System.out.println("Search end: " + searchEnd);
        System.out.println("Expected: [2022-12-02, 2023-01-02, 2023-02-02]");
        System.out.println(testRule.getDueDatesBetween(searchStart, searchEnd));
        /*ComboPooledDataSource source = new ComboPooledDataSource();
        source.setJdbcUrl("jdbc:sqlite:test.sqlite");
        Connection connection = source.getConnection();
        connection.createStatement().execute("DROP TABLE IF EXISTS users;");
        connection.createStatement().execute("""
                                             CREATE TABLE IF NOT EXISTS users (
                                                id TEXT PRIMARY KEY NOT NULL,
                                                username TEXT NOT NULL,
                                                passwordHash TEXT NOT NULL
                                             );
                                             """);
        connection.setAutoCommit(false);
        PreparedStatement statement = connection.prepareStatement("INSERT INTO users(id,username,passwordHash) VALUES(?,?,?);");
        ArrayList<User> users = new ArrayList<>();
        long start3 = System.nanoTime();
        for(int i = 0; i < 52; i++) {
            User user = new User("test@test.test", "Testing", "Testing", "test", "$2a$12$90l4lyVNnUCw8kkjsuMcOeHmDhF8j8HY/0u8wtbkFHTrZw9I1AyzO");
            users.add(user);
            statement.setString(1, user.getID().toString());
            statement.setString(2, user.getUsername());
            statement.setString(3, user.getPasswordHash());
            statement.addBatch();
        }
        statement.executeBatch();
        connection.commit();
        System.out.println("Time = " + (System.nanoTime() - start3));

        long start = System.currentTimeMillis();
        UUID id = users.get(26).getID();
        for(User user : users) {
            if(user.getID().equals(id)) {
                System.out.println("Time = " + (System.currentTimeMillis() - start));
            }
        }

        PreparedStatement statement2 = connection.prepareStatement("SELECT * FROM users WHERE id = ?");
        long start2 = System.currentTimeMillis();
        statement2.setString(1, id.toString());
        statement2.executeQuery();
        System.out.println("Time = " + (System.currentTimeMillis() - start2));*/
    }

}
