package io.sanger.henry.taskjourney;

import io.sanger.henry.taskjourney.api.TaskJourneyAPI;
import io.sanger.henry.taskjourney.api.journal.JournalInfo;
import io.sanger.henry.taskjourney.api.task.Task;
import io.sanger.henry.taskjourney.api.task.priority.Priority;
import io.sanger.henry.taskjourney.api.task.duerule.DueRule;
import io.sanger.henry.taskjourney.api.task.tag.Tag;
import io.sanger.henry.taskjourney.api.task.workspace.Workspace;
import io.sanger.henry.taskjourney.api.task.workspace.WorkspaceInfo;
import io.sanger.henry.taskjourney.api.user.User;
import io.sanger.henry.taskjourney.api.util.Utilities;
import net.lingala.zip4j.ZipFile;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import spark.Service;

import javax.servlet.MultipartConfigElement;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Stream;

import static io.sanger.henry.taskjourney.api.TaskJourneyAPI.getDatabase;
import static io.sanger.henry.taskjourney.api.util.ExceptionHandler.handleFatalException;
import static spark.Spark.*;

/**
 * The official TaskJourney server.
 * It uses the TaskJourney API, and hosts a webapp and REST API.
 */
public class TaskJourneyServer {

    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("EEE. M/dd/yyyy");

    public static void main(String[] args) {

        // Load configuration
        String host = "";
        int port = 0;
        try {
            JSONObject configObject = new JSONObject(Files.readString(Path.of("config.json")));
            host = configObject.getString("host");
            port = configObject.getInt("port");

            ipAddress(host);
            port(port);

            if(configObject.has("ssl") && configObject.getBoolean("ssl")) {
                String keystoreFile = Path.of(configObject.getString("keystoreFile")).toAbsolutePath().toString();
                String keystorePassword = configObject.getString("keystorePassword");
                secure(keystoreFile, keystorePassword, null, null);
            }

            if(configObject.has("redirect") && configObject.getBoolean("redirect")) {
                Service redirectServer = Service.ignite();
                redirectServer.ipAddress(host);
                redirectServer.port(configObject.getInt("redirectPort"));
                String redirectTarget = configObject.getString("redirectTarget");
                redirectServer.get("/*", (req, res) -> {
                    String url = req.url();
                    url = url.substring(7); // Remove http://
                    url = url.substring(url.indexOf('/')); // Remove domain
                    res.redirect(redirectTarget + url);
                    return "";
                });
            }
        } catch(JSONException je) {
            handleFatalException("Invalid config.json", je);
        } catch(IOException ioe) {
            handleFatalException("Unable to load config.json", ioe);
        }

        // Start and load everything
        try {
            TaskJourneyAPI.init(Path.of("data"));
        } catch(IOException | SQLException e) {
            handleFatalException("Error loading database", e);
        }


        // Set up static files
        if(TaskJourneyAPI.debugModeEnabled()) externalStaticFileLocation("src/main/resources/public");
        else staticFileLocation("public");


        // Set up routes
        before("/*", (req, res) -> {
            if(req.session(false) == null && req.cookie("rememberMe") != null) {
                String[] split = req.cookie("rememberMe").split("\\.");
                String lookup = split[0], validator = Utilities.getSHA256Hash(split[1].getBytes());
                User user = getDatabase().lookUpUser(lookup);
                if(user == null) return;
                for(Map.Entry<String, Long> record : user.getRememberMeRecords().entrySet()) {
                    if(record.getKey().equals(validator) && record.getValue() > System.currentTimeMillis()) {
                        req.session(true).attribute("id", user.getID());
                        req.session().attribute("passwordHash", user.getPasswordHash());
                    }
                }
            }
        });

        get("/", (req, res) -> {
            if(req.session(false) != null) return getFileContents("home.html");
            else return getFileContents("intro.html");
        });

        get("/signup", (req, res) -> {
            if(req.session(false) == null) return getFileContents("signup.html");
            else {
                res.redirect("/");
                return "";
            }
        });
        get("/signin", (req, res) -> {
            if(req.session(false) == null) return getFileContents("signin.html");
            else {
                res.redirect("/");
                return "";
            }
        });
        get("/signout", (req, res) -> {
            req.session().invalidate();
            res.removeCookie("rememberMe");
            res.redirect("/");
            return "";
        });

        get("/tasks", (req, res) -> {
            if(req.session(false) != null) return getFileContents("tasks.html");
            else res.redirect("/");
            return "";
        });

        get("/journals", (req, res) -> {
            if(req.session(false) != null) return getFileContents("journal/journals.html");
            else res.redirect("/");
            return "";
        });

        get("/journal", (req, res) -> {
            if(req.session(false) != null) return getFileContents("journal/journal.html");
            else res.redirect("/");
            return "";
        });

        get("/journal/edit", (req, res) -> {
            if(req.session(false) != null) return getFileContents("journal/edit.html");
            else res.redirect("/");
            return "";
        });

        path("/api/v1", () -> {
            AtomicReference<User> currentUser = new AtomicReference<>();
            AtomicReference<String> currentUserID = new AtomicReference<>();
            before("/*", (req, res) -> {
                if(req.url().endsWith("/signin") || req.url().endsWith("/signin/") || req.url().endsWith("/signup") || req.url().endsWith("/signup/")) return;

                if(req.session(false) == null) {
                    if(req.cookie("rememberMe") != null) {
                        String[] split = req.cookie("rememberMe").split("\\.");
                        String lookup = split[0], validator = Utilities.getSHA256Hash(split[1].getBytes());
                        User user = getDatabase().lookUpUser(lookup);
                        if(user == null) return;
                        for(Map.Entry<String, Long> record : user.getRememberMeRecords().entrySet()) {
                            if(record.getKey().equals(validator) && record.getValue() > System.currentTimeMillis()) {
                                req.session(true).attribute("id", user.getID());
                                req.session().attribute("passwordHash", user.getPasswordHash());
                            }
                        }
                    } else {
                        res.type("text/plain");
                        halt(401, "Not signed in");
                    }
                }

                currentUser.set(getDatabase().getUser((UUID) req.session().attribute("id")));
                if(!currentUser.get().getPasswordHash().equals(req.session().attribute("passwordHash"))) {
                    currentUser.set(null);
                    req.session().invalidate();
                    res.type("text/plain");
                    halt(401, "Invalid session");
                }
                currentUserID.set(currentUser.get().getID().toString());
            });

            // Sign up, sign in, and sign out
            post("/signup", (req, res) -> {
                res.type("application/json");
                JSONObject result = new JSONObject();
                if(req.session(false) != null) {
                    result.put("success", false);
                    result.put("reason", "Already signed in");
                    halt(400, result.toString());
                }
                try {
                    JSONObject requestBody = new JSONObject(req.body());
                    String email = requestBody.getString("email"), username = requestBody.getString("username");

                    if(!Utilities.isEmailValid(email)) {
                        result.put("success", false);
                        result.put("reason", "Invalid email");
                        halt(400, result.toString());
                    } else if(getDatabase().getUserByEmail(email) != null) {
                        result.put("success", false);
                        result.put("reason", "Email already taken");
                        halt(409, result.toString());
                    } else if(getDatabase().getUserByUsername(username) != null) {
                        result.put("success", false);
                        result.put("reason", "Username already taken");
                        halt(409, result.toString());
                    } else {
                        User user = new User(email,
                                requestBody.has("firstName") ? requestBody.getString("firstName") : null,
                                requestBody.has("lastName") ? requestBody.getString("lastName") : null,
                                username,
                                requestBody.getString("password"));
                        getDatabase().addNewUser(user);
                        result.put("success", true);
                        req.session(true).attribute("id", user.getID());
                        req.session().attribute("passwordHash", user.getPasswordHash());
                        if(requestBody.getBoolean("rememberMe")) res.cookie("/", "rememberMe", user.generateRememberMeString(), 31556926, false, true);
                    }
                } catch(JSONException je) {
                    result.put("success", false);
                    result.put("reason", "Invalid JSON: " + je.getMessage());
                    halt(400, result.toString());
                }
                return result.toString();
            });
            post("/signin", (req, res) -> {
                res.type("application/json");
                JSONObject result = new JSONObject();
                if(req.session(false) != null) {
                    result.put("success", false);
                    result.put("reason", "Already signed in");
                    halt(400, result.toString());
                }
                try {
                    JSONObject requestBody = new JSONObject(req.body());

                    // Try to get the user by the email
                    User user = getDatabase().getUserByEmail(requestBody.getString("emailOrUsername"));

                    // If that returns null, the given identifier wasn't an email, so try again with the username
                    if(user == null) user = getDatabase().getUserByUsername(requestBody.getString("emailOrUsername"));

                    // If that also returns null, there is no user who has the given identifier as the email or the username,
                    // so return an error with 404 status code
                    if(user == null) {
                        result.put("success", false);
                        result.put("reason", "User not found");
                        halt(404, result.toString());
                    }

                    // Verify the password. If it's invalid, return an error with 401 status code
                    if(Utilities.verify(requestBody.getString("password"), user.getPasswordHash())) {
                        result.put("success", true);
                        req.session(true).attribute("id", user.getID());
                        req.session(true).attribute("passwordHash", user.getPasswordHash());
                        if(requestBody.getBoolean("rememberMe")) res.cookie("/", "rememberMe", user.generateRememberMeString(), 31556926, false, true);
                    } else {
                        result.put("success", false);
                        result.put("reason", "Invalid password");
                        halt(401, result.toString());
                    }
                } catch(JSONException je) {
                    result.put("success", false);
                    result.put("reason", "Invalid JSON: " + je.getMessage());
                    halt(400, result.toString());
                }
                return result.toString();
            });
            post("/signout", (req, res) -> {
                req.session().invalidate();
                res.removeCookie("rememberMe");
                res.redirect("/");
                return "";
            });

            get("/whoami", (req, res) -> {
                JSONObject userObject = new JSONObject();
                userObject.put("id", currentUser.get().getID());
                String firstName = currentUser.get().getFirstName(), lastName = currentUser.get().getLastName();
                if(firstName != null) userObject.put("firstName", firstName);
                if(lastName != null) userObject.put("lastName", lastName);
                if(firstName != null || lastName != null)
                    userObject.put("fullName", (firstName != null ? firstName : "") + (lastName != null ? " " + lastName : ""));
                userObject.put("username", currentUser.get().getUsername());
                res.type("application/json");
                return userObject.toString();
            });

            get("/defaultWorkspace", (req, res) -> currentUser.get().getDefaultWorkspace());

            get("/workspaces", (req, res) -> {
                JSONArray workspacesArray = new JSONArray();
                for(WorkspaceInfo info : getDatabase().getWorkspaces(currentUser.get())) {
                    workspacesArray.put(info.toJSONObject());
                }
                res.type("application/json");
                return workspacesArray;
            });

            path("/workspace/:workspace", () -> {
                AtomicReference<Workspace> workspace = new AtomicReference<>();
                before("/*", (req, res) -> {
                    res.type("text/plain");

                    // Convert the workspace ID to a UUID
                    UUID workspaceID = null;
                    try {
                        workspaceID = UUID.fromString(req.params("workspace"));
                    } catch(IllegalArgumentException e) {
                        halt(400, "Invalid workspace ID");
                    }

                    workspace.set(getDatabase().getWorkspace(workspaceID));

                    // If the workspace doesn't exist, return a 404 error
                    if(workspace.get() == null) {
                        halt(404, "Workspace not found");
                    }

                    // Ensure the user accessing the workspace has permission to view tasks
                    if(!workspace.get().canView(currentUser.get())) {
                        halt(403, "Access denied");
                    }
                });

                get("/tasks", (req, res) -> {
                    try {
                        JSONObject responseObject = new JSONObject();
                        JSONArray tasks = new JSONArray();

                        boolean showingTaskNav = false;
                        int pastDueCount = 0, nowCount = 0, laterCount = 0;

                        switch(req.queryParams("return")) {
                            case "all" -> {
                                for(Task task : workspace.get().getTasks()) {
                                    tasks.put(task.toJSONObject(LocalDate.now(), false));
                                }
                            }
                            case "forDate" -> {
                                LocalDate date = LocalDate.parse(req.queryParams("date"));
                                String due = req.queryParams("due");
                                showingTaskNav = true;
                                ArrayList<UUID> pastDue = new ArrayList<>(),
                                        now = new ArrayList<>(),
                                        later = new ArrayList<>();

                                for(Task task : workspace.get().getTasks()) {
                                    UUID id = task.getID();
                                    if(task.isDue(date)) {
                                        if(due.equals("now")) {
                                            tasks.put(task.toJSONObject(date, true));
                                            now.add(task.getID());
                                        }
                                        nowCount++;
                                    }
                                    if(task.isDueBefore(date) && !(pastDue.contains(id) || now.contains(id) || later.contains(id))) {
                                        if(due.equals("pastDue")) {
                                            tasks.put(task.toJSONObject(date, true));
                                            pastDue.add(task.getID());
                                        }
                                        pastDueCount++;
                                    }
                                    if(task.isDueAfter(date) && !(pastDue.contains(id) || now.contains(id) || later.contains(id))) {
                                        if(due.equals("later")) {
                                            tasks.put(task.toJSONObject(date, true));
                                            later.add(task.getID());
                                        }
                                        laterCount++;
                                    }
                                }
                            }
                            /* TODO
                            case "forRange" -> {

                            }
                            */
                            default -> halt(400, "Invalid \"return\" value");
                        }

                        if(showingTaskNav) {
                            responseObject.put("pastDueCount", pastDueCount);
                            responseObject.put("nowCount", nowCount);
                            responseObject.put("laterCount", laterCount);
                        }
                        responseObject.put("tasks", tasks);
                        res.type("application/json");
                        return responseObject;
                    } catch(JSONException | DateTimeParseException e) {
                        halt(400, e.getMessage());
                    }
                    throw new IllegalStateException();
                });

                // TODO
                get("/lists", (req, res) -> {
                    res.type("application/json");
                    return "[]";
                });

                get("/tags", (req, res) -> {
                    JSONArray tagArray = new JSONArray();
                    for(Tag tag : workspace.get().getTags())
                        tagArray.put(tag.toJSONObject());
                    res.type("application/json");
                    return tagArray.toString();
                });

                path("/edit", () -> {
                    before("/*", (req, res) -> {
                        if(!workspace.get().canEdit(currentUser.get())) halt(403);
                    });

                    post("/newTask", (req, res) -> {
                        JSONObject requestObject = new JSONObject(req.body());
                        Task task = new Task(requestObject.getString("name"));
                        workspace.get().addTask(task);
                        res.type("application/json");
                        return task.toJSONObject().toString();
                    });

                    path("/task/:task", () -> {
                        AtomicReference<Task> task = new AtomicReference<>();
                        before("/*", (req, res) -> {

                            // Convert the task ID to a UUID
                            UUID taskID = null;
                            try {
                                taskID = UUID.fromString(req.params("task"));
                            } catch(IllegalArgumentException e) {
                                halt(400, "Invalid workspace ID");
                            }

                            task.set(workspace.get().getTask(taskID));

                            // If the task doesn't exist, return a 404 error
                            if(task.get() == null) halt(404, "Task not found");
                        });

                        post("/", (req, res) -> {
                            JSONObject requestObject = new JSONObject(req.body());
                            if(requestObject.has("name")) task.get().setName(requestObject.getString("name"));
                            if(requestObject.has("description")) task.get().setDescription(requestObject.getString("description"));
                            if(requestObject.has("dueRule")) task.get().setDueRule(DueRule.fromJSONObject(requestObject.getJSONObject("dueRule")));
                            if(requestObject.has("priority")) task.get().setPriority(Priority.valueOf(requestObject.getString("priority")));
                            return true;
                        });

                        post("/complete", (req, res) -> {
                            task.get().setCompleted(true, LocalDate.parse(req.body()));
                            return true;
                        });

                        post("/uncomplete", (req, res) -> {
                            task.get().setCompleted(false, LocalDate.parse(req.body()));
                            return true;
                        });

                        post("/skip", (req, res) -> {
                            task.get().setSkipped(true, LocalDate.parse(req.body()));
                            return true;
                        });

                        post("/unskip", (req, res) -> {
                            task.get().setSkipped(false, LocalDate.parse(req.body()));
                            return true;
                        });

                        // Moves a task to the trash
                        delete("/trash", (req, res) -> {
                            workspace.get().trashTask(task.get());
                            return true;
                        });

                        // Permanently deletes a task
                        delete("/delete", (req, res) -> {
                            workspace.get().deleteTask(task.get());
                            return true;
                        });

                        // Restores a task from the trash
                        post("/restore", (req, res) -> {
                            workspace.get().restoreTask(task.get());
                            return true;
                        });
                    }); // End of task routes

                    delete("/emptyTrash", (req, res) -> {
                        if(!workspace.get().canEdit(currentUser.get())) halt(403);
                        workspace.get().emptyTrash();
                        res.type("text/plain");
                        return true;
                    });

                    post("/newTag", (req, res) -> {
                        if(!workspace.get().canEdit(currentUser.get())) halt(403);
                        JSONObject requestObject = new JSONObject(req.body());
                        Tag tag = new Tag(requestObject.getString("name"), requestObject.getString("colorCode"));
                        workspace.get().addTag(tag);
                        res.type("application/json");
                        return tag.toJSONObject().toString();
                    });

                    after("/*", (req, res) -> workspace.get().save());
                }); // End of workspace edit routes
            }); // End of workspace routes

            get("/journals", (req, res) -> {
                JSONArray journalsArray = new JSONArray();
                for(JournalInfo info : getDatabase().getJournals(currentUser.get())) {
                    journalsArray.put(info.toJSONObject());
                }
                res.type("application/json");
                return journalsArray;
            });

            post("/newJournal", (req, res) -> {
                String journalID = UUID.randomUUID().toString();
                String title = req.body();
                LocalDateTime lastUpdated = LocalDateTime.now();
                Path journalPath = getDatabase().getRoot().resolve("journals").resolve(journalID);
                Files.createDirectories(journalPath.resolve("entries"));
                JSONObject info = new JSONObject();
                info.put("title", title);
                info.put("lastUpdated", lastUpdated);
                info.put("lastUpdatedText", Utilities.lastUpdatedTimeFormat(lastUpdated));
                info.put("entryCount", 0);
                Files.writeString(journalPath.resolve("info.json"), info.toString());
                getDatabase().addJournal(journalID, title, lastUpdated.toString(), 0, currentUserID.get());
                return true;
            });

            post("/importJournal", (req, res) -> {

                // Get the uploaded zip
                req.attribute("org.eclipse.jetty.multipartConfig", new MultipartConfigElement("/tmp"));
                InputStream fileStream = req.raw().getPart("file").getInputStream();
                Path tempFile = Files.createTempFile("TaskJourney", null);
                Files.write(tempFile, fileStream.readAllBytes());

                // Extract the zip
                Path tempDirectory = Files.createTempDirectory("TaskJourney");
                ZipFile tempZip = new ZipFile(tempFile.toFile());
                tempZip.extractAll(tempDirectory.toAbsolutePath().toString());
                tempZip.close();

                // Add the journal to the database
                String journalID = UUID.randomUUID().toString();
                Path journalPath = getDatabase().getRoot().resolve("journals").resolve(journalID);
                JSONObject info = new JSONObject(Files.readString(tempDirectory.resolve("info.json")));
                String title = info.getString("title");
                Files.move(tempDirectory, journalPath);
                getDatabase().addJournal(journalID, title, info.getString("lastUpdated"), info.getInt("entryCount"), currentUserID.get());

                // Build the Lucene index
                IndexWriter writer = getDatabase().getJournalIndexWriter(journalID);
                Stream<Path> entries = Files.list(journalPath.resolve("entries"));
                entries.forEach(entry -> {
                    try {
                        JSONObject entryObject = new JSONObject(Files.readString(entry));
                        String content = Utilities.clean(entryObject.getString("content"));
                        LocalDate date = LocalDate.parse(entryObject.getString("date"));
                        Document document = new Document();
                        document.add(new StringField("id", entry.getFileName().toString().substring(0, 36), Field.Store.YES));
                        document.add(new TextField("title", entryObject.getString("title"), Field.Store.YES));
                        document.add(new TextField("content", content, Field.Store.NO));
                        document.add(new TextField("contentPreview", Utilities.trim(content), Field.Store.YES));
                        document.add(new TextField("date", date.toString(), Field.Store.YES));
                        document.add(new TextField("dateText", date.format(DATE_FORMAT), Field.Store.YES));
                        writer.addDocument(document);
                    } catch(IOException e) {
                        throw new RuntimeException(e);
                    }
                });
                entries.close();
                writer.close();

                // Return the journal's new ID
                res.type("text/plain");
                return journalID;
            });

            path("/journal/:journal", () -> {

                AtomicReference<String> journalID = new AtomicReference<>();
                AtomicReference<Path> journalPath = new AtomicReference<>();
                before("/*", (req, res) -> {
                    res.type("text/plain");
                    journalID.set(req.params("journal"));
                    if(!getDatabase().canViewJournal(journalID.get(), currentUserID.get())) halt(403, "Access denied");
                    journalPath.set(getDatabase().getRoot().resolve("journals").resolve(req.params("journal")));
                    if(!Files.isDirectory(journalPath.get())) halt(404, "Journal not found");
                });

                get("/", (req, res) -> {
                    res.type("application/json");
                    return Files.readString(journalPath.get().resolve("info.json"));
                });

                final HashMap<String, Float> BOOSTS = new HashMap<>();
                BOOSTS.put("title", 2.0f);
                BOOSTS.put("content", 1.0f);
                final String[] FIELDS = new String[] {"title", "content"};

                get("/entries", (req, res) -> {

                    // Get offset and count
                    int offset = 0;
                    if(req.queryParams("offset") != null) {
                        try {
                            offset = Integer.parseInt(req.queryParams("offset"));
                        } catch(NumberFormatException e) {
                            offset = -1;
                        }
                    }
                    int count = -1;
                    if(req.queryParams("count") != null) {
                        count = Integer.parseInt(req.queryParams("count"));
                    }

                    // Construct the query
                    Query query;
                    if(req.queryParams("q") != null) {
                        String q = req.queryParams("q");
                        MultiFieldQueryParser parser = new MultiFieldQueryParser(FIELDS, new StandardAnalyzer(), BOOSTS);
                        parser.setDefaultOperator(QueryParser.Operator.AND);
                        BooleanQuery.Builder queryBuilder = new BooleanQuery.Builder()
                                .add(new FuzzyQuery(new Term("title", q), 1), BooleanClause.Occur.SHOULD)
                                .add(new FuzzyQuery(new Term("content", q), 1), BooleanClause.Occur.SHOULD)
                                .add(parser.parse(QueryParser.escape(q)), BooleanClause.Occur.SHOULD);
                        query = queryBuilder.build();
                    } else {
                        query = new MatchAllDocsQuery();
                    }

                    // Do the search
                    JSONArray entries = new JSONArray();
                    ArrayList<JSONObject> tempEntries = new ArrayList<>();
                    IndexSearcher searcher = getDatabase().getJournalIndexSearcher(journalID.get());
                    TopDocs hits = searcher.search(query, Integer.MAX_VALUE);
                    for(int i = 0; i < hits.scoreDocs.length; i++) {
                        Document doc = searcher.doc(hits.scoreDocs[i].doc);
                        JSONObject entry = new JSONObject();
                        entry.put("id", doc.get("id"));
                        entry.put("title", doc.get("title"));
                        entry.put("contentPreview", doc.get("contentPreview"));
                        entry.put("date", doc.get("date"));
                        entry.put("dateText", doc.get("dateText"));
                        tempEntries.add(entry);
                    }

                    // Sort and limit the results
                    tempEntries.sort(Comparator.comparing(o -> LocalDate.parse(((JSONObject) o).getString("date"))).reversed());
                    if(offset == -1) {
                        for(int i = 0; i < tempEntries.size(); i++) {
                            JSONObject entry = tempEntries.get(i);
                            if(entry.getString("id").equals(req.queryParams("offset")))
                                offset = i - (count / 2); // Allow setting offset based on entry ID instead of index
                        }
                        if(offset < 0) offset = 0;
                    }
                    int end;
                    if(count != -1) end = Math.min(offset + count, tempEntries.size());
                    else end = tempEntries.size();
                    for(int i = offset; i < end; i++) entries.put(tempEntries.get(i));

                    // Return the results
                    res.type("application/json");
                    return entries;
                });

                get("/entry/:id", (req, res) -> {
                    res.type("application/json");
                    return Files.readString(journalPath.get().resolve("entries").resolve(req.params("id") + ".json"));
                });

                get("/randomEntry", (req, res) -> {
                    File[] list = journalPath.get().resolve("entries").toFile().listFiles();
                    assert list != null;
                    res.type("text/plain");
                    return list[ThreadLocalRandom.current().nextInt(list.length)].getName().substring(0, 36);
                });

                get("/export", (req, res) -> {
                    Path tempFile = Files.createTempFile("TaskJourney", null);
                    Files.delete(tempFile);
                    try(ZipFile backupZip = new ZipFile(tempFile.toFile())) {
                        backupZip.addFolder(journalPath.get().resolve("entries").toFile());
                        backupZip.addFile(journalPath.get().resolve("info.json").toFile());
                    }
                    byte[] exportBytes = Files.readAllBytes(tempFile);
                    Files.delete(tempFile);
                    res.header("Content-Disposition", "attachment; filename=\"journal-export-" + LocalDate.now() + ".zip\"");
                    res.type("application/zip");
                    return exportBytes;
                });

                path("/edit", () -> {
                    before("/*", (req, res) -> {
                        if(!getDatabase().canEditJournal(journalID.get(), currentUserID.get())) halt(403, "Access denied");
                    });

                    post("/newEntry", (req, res) -> {
                        String entryID = UUID.randomUUID().toString();
                        String lastUpdated = LocalDateTime.now().toString();
                        LocalDate now = LocalDate.now();
                        JSONObject entryObject = new JSONObject();
                        entryObject.put("title", "");
                        entryObject.put("content", "");
                        entryObject.put("date", now.toString());
                        entryObject.put("created", lastUpdated);
                        entryObject.put("lastUpdated", lastUpdated);
                        Files.writeString(
                                journalPath.get().resolve("entries").resolve(entryID + ".json"),
                                entryObject.toString()
                        );
                        IndexWriter writer = getDatabase().getJournalIndexWriter(journalID.get());
                        Document document = new Document();
                        document.add(new StringField("id", entryID, Field.Store.YES));
                        document.add(new TextField("title", "", Field.Store.YES));
                        document.add(new TextField("content", "", Field.Store.NO));
                        document.add(new TextField("contentPreview", "", Field.Store.YES));
                        document.add(new TextField("date", now.toString(), Field.Store.YES));
                        document.add(new TextField("dateText", now.format(DATE_FORMAT), Field.Store.YES));
                        writer.addDocument(document);
                        writer.close();
                        getDatabase().updateJournal(journalID.get(), 1);
                        return entryID;
                    });

                    patch("/entry/:id", (req, res) -> {
                        String entryID = req.params("id");
                        Path entryPath = journalPath.get().resolve("entries").resolve(entryID + ".json");
                        JSONObject entryObject = new JSONObject(Files.readString(entryPath));
                        JSONObject editObject = new JSONObject(req.body());
                        LocalDate date = LocalDate.parse(editObject.getString("date"));
                        entryObject.put("title", editObject.getString("title"));
                        entryObject.put("content", editObject.getString("content"));
                        entryObject.put("date", date);
                        if(editObject.has("started")) entryObject.put("started", OffsetDateTime.parse(editObject.getString("started")).atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
                        if(editObject.has("finished")) entryObject.put("finished", OffsetDateTime.parse(editObject.getString("finished")).atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
                        entryObject.put("lastUpdated", LocalDateTime.now());
                        Files.writeString(entryPath, entryObject.toString());
                        IndexWriter writer = getDatabase().getJournalIndexWriter(journalID.get());
                        Document document = new Document();
                        document.add(new StringField("id", entryID, Field.Store.YES));
                        document.add(new TextField("title", editObject.getString("title"), Field.Store.YES));
                        document.add(new TextField("content", Utilities.clean(editObject.getString("content")), Field.Store.NO));
                        document.add(new TextField("contentPreview", Utilities.getContentPreview(editObject.getString("content")), Field.Store.YES));
                        document.add(new TextField("date", date.toString(), Field.Store.YES));
                        document.add(new TextField("dateText", date.format(DATE_FORMAT), Field.Store.YES));
                        writer.updateDocument(new Term("id", entryID), document);
                        writer.close();
                        getDatabase().updateJournal(journalID.get(), 0);
                        res.type("text/plain");
                        return true;
                    });

                    delete("/entry/:id/delete", (req, res) -> {
                        Files.delete(journalPath.get().resolve("entries").resolve(req.params("id") + ".json"));
                        IndexWriter writer = getDatabase().getJournalIndexWriter(journalID.get());
                        writer.deleteDocuments(new Term("id", req.params("id")));
                        writer.close();
                        getDatabase().updateJournal(journalID.get(), -1);
                        res.type("text/plain");
                        return true;
                    });
                }); // End of journal edit routes

            }); // End of journal routes

        }); // End of API routes

        System.out.println("TaskJourney Server v1.0.0");
        System.out.println("Listening on " + host + ":" + port);
        System.out.println("Press Ctrl+C to exit.");
        new Scanner(System.in).nextLine();
        System.exit(0);
    }

    private static String getFileContents(String filename) throws IOException {
        if(TaskJourneyAPI.debugModeEnabled()) {
            return Files.readString(Path.of("src/main/resources/public/" + filename));
        } else {
            try(InputStream stream = TaskJourneyServer.class.getResourceAsStream("/public/" + filename)) {
                if(stream == null) throw new FileNotFoundException("/public/" + filename);
                else return new String(stream.readAllBytes());
            } catch(IOException e) {
                handleFatalException("Unable to locate resource: " + filename, e);
            }
        }
        return null;
    }

}
