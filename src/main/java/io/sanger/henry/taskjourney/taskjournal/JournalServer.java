package io.sanger.henry.taskjourney.taskjournal;

import io.sanger.henry.taskjourney.api.journal.Journal;
import io.sanger.henry.taskjourney.api.journal.JournalEntry;
import io.sanger.henry.taskjourney.api.journal.JournalManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.Objects;
import java.util.Random;

import static spark.Spark.*;

public class JournalServer {

    public static final boolean DEBUG_MODE = true;

    public static final DateTimeFormatter DATE_FORMAT = DateTimeFormatter.ofPattern("EEE. M/dd/yyyy");

    public static void main(String[] args) throws IOException {

        // Load journals
        JournalManager.load();

        // Set up the server
        JSONObject config = new JSONObject(Files.readString(Path.of("config.json")));

        ipAddress(config.getString("host"));
        port(config.getInt("port"));

        if(DEBUG_MODE) externalStaticFileLocation("src/main/resources/public");
        else staticFileLocation("/public");

        get("/io/sanger/henry/taskjourney/taskjournal", (req, res) -> {
            res.type("text/html");
            return getFile("journal.html");
        });
        get("/edit", (req, res) -> {
            res.type("text/html");
            return getFile("edit.html");
        });

        path("/api/v1", () -> {
            get("/journals", (req, res) -> {
                JSONArray journals = new JSONArray();
                for(int i = 0; i < JournalManager.getJournals().size(); i++) {
                    Journal journal = JournalManager.getJournals().get(i);
                    JSONObject journalObject = orderedJSONObject();
                    journalObject.put("title", journal.getTitle());
                    journalObject.put("lastUpdated", journal.getLastUpdated());
                    journalObject.put("lastUpdatedText", DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(journal.getLastUpdated()));
                    journalObject.put("entryCount", journal.getEntries().size());
                    journalObject.put("uniqueID", journal.getUniqueID());
                    journals.put(journalObject);
                }
                res.type("application/json");
                return journals.toString();
            });

            get("/io/sanger/henry/taskjourney/taskjournal/:uniqueID", (req, res) -> {
                Journal journal = JournalManager.getJournal(req.params("uniqueID"));
                if(journal == null) halt(404, "Journal not found");
                JSONObject journalObject = orderedJSONObject();
                journalObject.put("title", journal.getTitle());
                journalObject.put("lastUpdated", journal.getLastUpdated().toString());
                journalObject.put("lastUpdatedText", DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(journal.getLastUpdated()));
                journalObject.put("entryCount", journal.getEntries().size());
                res.type("application/json");
                return journalObject.toString();
            });

            get("/io/sanger/henry/taskjourney/taskjournal/*/entries", (req, res) -> {
                Journal journal = JournalManager.getJournal(req.splat()[0]);
                if(journal == null) halt(404, "Journal not found");
                JSONArray entries = new JSONArray();
                for(JournalEntry entry : journal.getEntries()) {
                    JSONObject entryObject = orderedJSONObject();
                    entryObject.put("title", entry.getTitle());
                    entryObject.put("contentPreview", entry.getContentPreview());
                    entryObject.put("date", entry.getDate().toString());
                    entryObject.put("dateText", DATE_FORMAT.format(entry.getDate()));
                    entryObject.put("created", entry.getCreated().toString());
                    entryObject.put("started", (entry.getStarted() == null ? null : entry.getStarted().toString()));
                    entryObject.put("finished", (entry.getFinished() == null ? null : entry.getFinished().toString()));
                    entryObject.put("lastUpdated", entry.getLastUpdated().toString());
                    entryObject.put("uniqueID", entry.getUniqueID());
                    entries.put(entryObject);
                }
                res.type("application/json");
                return entries.toString();
            });

            get("/io/sanger/henry/taskjourney/taskjournal/*/entries/*", (req, res) -> {
                Journal journal = JournalManager.getJournal(req.splat()[0]);
                if(journal == null) halt(404, "Journal not found");
                int numEntries = Integer.parseInt(req.splat()[1]);
                JSONArray entries = new JSONArray();
                for(int i = 0; i < Math.min(numEntries, journal.getEntries().size()); i++) {
                    JournalEntry entry = journal.getEntries().get(i);
                    JSONObject entryObject = orderedJSONObject();
                    entryObject.put("title", entry.getTitle());
                    entryObject.put("contentPreview", entry.getContentPreview());
                    entryObject.put("date", entry.getDate().toString());
                    entryObject.put("dateText", DATE_FORMAT.format(entry.getDate()));
                    entryObject.put("created", entry.getCreated().toString());
                    entryObject.put("started", (entry.getStarted() == null ? null : entry.getStarted().toString()));
                    entryObject.put("finished", (entry.getFinished() == null ? null : entry.getFinished().toString()));
                    entryObject.put("lastUpdated", entry.getLastUpdated().toString());
                    entryObject.put("uniqueID", entry.getUniqueID());
                    entries.put(entryObject);
                }
                res.type("application/json");
                return entries.toString();
            });

            get("/io/sanger/henry/taskjourney/taskjournal/*/entry/*", (req, res) -> {
                Journal journal = JournalManager.getJournal(req.splat()[0]);
                if(journal == null) halt(404, "Journal not found");
                JournalEntry entry = journal.getEntry(req.splat()[1]);
                if(entry == null) halt(404, "Entry not found");
                JSONObject entryObject = orderedJSONObject();
                entryObject.put("title", entry.getTitle());
                entryObject.put("contentPreview", entry.getContentPreview());
                entryObject.put("content", entry.getContent());
                entryObject.put("date", entry.getDate().toString());
                entryObject.put("dateText", DATE_FORMAT.format(entry.getDate()));
                entryObject.put("created", entry.getCreated().toString());
                entryObject.put("started", (entry.getStarted() == null ? null : entry.getStarted().toString()));
                entryObject.put("finished", (entry.getFinished() == null ? null : entry.getFinished().toString()));
                entryObject.put("lastUpdated", entry.getLastUpdated().toString());
                res.type("application/json");
                return entryObject.toString();
            });

            Random random = new Random();
            get("/io/sanger/henry/taskjourney/taskjournal/*/randomEntry", (req, res) -> {
                Journal journal = JournalManager.getJournal(req.splat()[0]);
                if(journal == null) halt(404, "Journal not found");
                JournalEntry entry = null;
                if(journal.getEntries().size() == 0) {
                    halt(400, "No entries in journal");
                } else if(journal.getEntries().size() == 1) {
                    entry = journal.getEntries().get(0);
                } else {
                    entry = journal.getEntries().get(random.nextInt(0, journal.getEntries().size() - 1));
                }
                JSONObject entryObject = orderedJSONObject();
                entryObject.put("title", entry.getTitle());
                entryObject.put("contentPreview", entry.getContentPreview());
                entryObject.put("content", entry.getContent());
                entryObject.put("date", entry.getDate().toString());
                entryObject.put("dateText", DATE_FORMAT.format(entry.getDate()));
                entryObject.put("created", entry.getCreated().toString());
                entryObject.put("started", (entry.getStarted() == null ? null : entry.getStarted().toString()));
                entryObject.put("finished", (entry.getFinished() == null ? null : entry.getFinished().toString()));
                entryObject.put("lastUpdated", entry.getLastUpdated().toString());
                entryObject.put("uniqueID", entry.getUniqueID());
                res.type("application/json");
                return entryObject.toString();
            });


            post("/newJournal", (req, res) -> {
                String title = req.body();
                LocalDateTime lastUpdated = LocalDateTime.now();
                Journal journal = new Journal(title);
                journal.setLastUpdated(lastUpdated);
                JournalManager.getJournals().add(journal);

                Path journalPath = Path.of("journals").resolve(journal.getUniqueID());
                Files.createDirectories(journalPath.resolve("entries"));
                JSONObject info = orderedJSONObject();
                info.put("title", title);
                info.put("lastUpdated", lastUpdated.toString());
                Files.writeString(journalPath.resolve("info.json"), info.toString());
                return true;
            });

            patch("/io/sanger/henry/taskjourney/taskjournal/*/edit", (req, res) -> ""); // TODO
            delete("/io/sanger/henry/taskjourney/taskjournal/*/delete", (req, res) -> ""); // TODO

            post("/io/sanger/henry/taskjourney/taskjournal/*/newEntry", (req, res) -> {
                Journal journal = JournalManager.getJournal(req.splat()[0]);
                if(journal == null) halt(404, "Journal not found");
                JournalEntry entry = new JournalEntry("", "", journal.getUniqueID());
                journal.getEntries().add(0, entry);
                entry.save();
                journal.updated();
                return entry.getUniqueID();
            });

            patch("/io/sanger/henry/taskjourney/taskjournal/*/entry/*/edit", (req, res) -> {
                try {
                    Journal journal = JournalManager.getJournal(req.splat()[0]);
                    if(journal == null) halt(404, "Journal not found");
                    JournalEntry entry = journal.getEntry(req.splat()[1]);
                    if(entry == null) halt(404, "Entry not found");
                    JSONObject entryObject = new JSONObject(req.body());
                    entry.setTitle(entryObject.getString("title"));
                    entry.setContent(entryObject.getString("content"));
                    LocalDate prevDate = entry.getDate();
                    if(entryObject.has("date")) entry.setDate(LocalDate.parse(entryObject.getString("date")));
                    if(!prevDate.isEqual(entry.getDate())) journal.sort();
                    if(entryObject.has("started")) entry.setStarted(OffsetDateTime.parse(entryObject.getString("started")).atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
                    if(entryObject.has("finished")) entry.setFinished(OffsetDateTime.parse(entryObject.getString("finished")).atZoneSameInstant(ZoneId.systemDefault()).toLocalDateTime());
                    entry.setLastUpdated(LocalDateTime.now());
                    entry.save();
                    journal.updated();
                    return true;
                } catch(JSONException exception) {
                    halt(400, "Invalid JSON");
                    return false;
                }
            });

            delete("/io/sanger/henry/taskjourney/taskjournal/*/entry/*/delete", (req, res) -> {
                Journal journal = JournalManager.getJournal(req.splat()[0]);
                if(journal == null) halt(404, "Journal not found");
                journal.getEntries().removeIf(entry -> entry.getUniqueID().equals(req.splat()[1]));
                Files.delete(Path.of("journals").resolve(req.splat()[0]).resolve("entries").resolve(req.splat()[1] + ".json"));
                journal.updated();
                return true;
            });
        });
    }

    // https://stackoverflow.com/a/35797158/5905216
    public static JSONObject orderedJSONObject() {
        try {
            JSONObject object = new JSONObject();
            Field map = object.getClass().getDeclaredField("map");
            map.setAccessible(true);
            map.set(object, new LinkedHashMap<>());
            map.setAccessible(false);
            return object;
        } catch(Exception e) {
            e.printStackTrace();
            return new JSONObject();
        }
    }

    public static void deleteDirectory(Path directory) throws IOException {
        if(Files.notExists(directory)) return;
        //noinspection ResultOfMethodCallIgnored
        Files.walk(directory)
                .sorted(Comparator.reverseOrder())
                .map(Path::toFile)
                .forEach(File::delete);
    }

    public static String getFile(String path) throws IOException {
        if(DEBUG_MODE) {
            return Files.readString(Path.of("src/main/resources/public/" + path));
        } else {
            InputStream stream = Objects.requireNonNull(JournalServer.class.getResourceAsStream("/public/" + path), "Couldn't find resource: /public/" + path);
            String content = new String(stream.readAllBytes());
            stream.close();
            return content; // TODO If debug mode is off, make this method return the file from memory
        }
    }

}
