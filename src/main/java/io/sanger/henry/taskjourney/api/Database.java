package io.sanger.henry.taskjourney.api;

import com.mchange.v2.c3p0.ComboPooledDataSource;
import io.sanger.henry.taskjourney.api.journal.JournalInfo;
import io.sanger.henry.taskjourney.api.task.workspace.Workspace;
import io.sanger.henry.taskjourney.api.task.workspace.WorkspaceInfo;
import io.sanger.henry.taskjourney.api.user.User;
import io.sanger.henry.taskjourney.api.util.Utilities;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.miscellaneous.PerFieldAnalyzerWrapper;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.FSDirectory;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;
import java.util.stream.Collectors;

public class Database {

    private final Path root;

    private final ComboPooledDataSource source;


    public Database(Path root) throws IOException, SQLException {
        this.root = root;
        this.source = new ComboPooledDataSource();
        source.setJdbcUrl("jdbc:sqlite:" + root.resolve("database.sqlite").toAbsolutePath());
        //source.setMaxStatements(50);

        Connection connection = source.getConnection();
        connection.createStatement().execute("""
                                             CREATE TABLE IF NOT EXISTS users (
                                                id TEXT PRIMARY KEY NOT NULL,
                                                email TEXT NOT NULL,
                                                username TEXT NOT NULL,
                                                lookup TEXT
                                             );
                                             """);
        connection.createStatement().execute("""
                                             CREATE TABLE IF NOT EXISTS workspaces (
                                                id TEXT PRIMARY KEY NOT NULL,
                                                name TEXT NOT NULL,
                                                owner TEXT NOT NULL,
                                                canView TEXT NOT NULL,
                                                canEdit TEXT NOT NULL,
                                                canManage TEXT NOT NULL
                                             );
                                             """);
        connection.createStatement().execute("""
                                             CREATE TABLE IF NOT EXISTS journals (
                                                id TEXT PRIMARY KEY NOT NULL,
                                                title TEXT NOT NULL,
                                                lastUpdated TEXT NOT NULL,
                                                entryCount INTEGER NOT NULL,
                                                owner TEXT NOT NULL,
                                                canView TEXT NOT NULL,
                                                canEdit TEXT NOT NULL,
                                                canManage TEXT NOT NULL
                                             );
                                             """);
        /* TODO
        connection.createStatement().execute("""
                                             CREATE TABLE IF NOT EXISTS destroyDates (
                                                id TEXT PRIMARY KEY NOT NULL,
                                                type TEXT NOT NULL,
                                                date TEXT NOT NULL,
                                             );
                                             """);
         */
        connection.close();

        Files.createDirectories(root.resolve("users"));
        Files.createDirectories(root.resolve("workspaces"));
        Files.createDirectories(root.resolve("journals"));
    }


    public Path getRoot() {
        return root;
    }

    public void addNewUser(User user) throws IOException, SQLException {
        Path dataPath = root.resolve("users").resolve(user.getID().toString());
        Files.createDirectories(dataPath);
        Files.writeString(dataPath.resolve("data.json"), user.toJSONObject().toString());

        Connection connection = source.getConnection();
        PreparedStatement statement = connection.prepareStatement("INSERT INTO users(id,email,username,lookup) VALUES(?,?,?,?);");
        statement.setString(1, user.getID().toString());
        statement.setString(2, user.getEmail());
        statement.setString(3, user.getUsername());
        statement.setString(4, user.getRememberMeLookup());
        statement.execute();
        connection.close();
    }

    public void addNewWorkspace(Workspace workspace) throws IOException, SQLException {
        Path dataPath = root.resolve("workspaces").resolve(workspace.getID().toString());
        Files.createDirectories(dataPath);
        Files.writeString(dataPath.resolve("data.json"), workspace.toJSONObject().toString());

        Connection connection = source.getConnection();
        PreparedStatement statement = connection.prepareStatement("INSERT INTO workspaces(id,name,owner,canView,canEdit,canManage) VALUES(?,?,?,?,?,?);");
        statement.setString(1, workspace.getID().toString());
        statement.setString(2, workspace.getName());
        statement.setString(3, workspace.getOwner().toString());
        statement.setString(4, workspace.getCanView().stream().map(UUID::toString).collect(Collectors.joining(",")));
        statement.setString(5, workspace.getCanEdit().stream().map(UUID::toString).collect(Collectors.joining(",")));
        statement.setString(6, workspace.getCanManage().stream().map(UUID::toString).collect(Collectors.joining(",")));
        statement.execute();
        connection.close();
    }

    public void addJournal(String journalID,
                           String title,
                           String lastUpdated,
                           int entryCount,
                           String ownerID) throws SQLException {
        Connection connection = source.getConnection();
        PreparedStatement statement = connection.prepareStatement("INSERT INTO journals(id,title,lastUpdated,entryCount,owner,canView,canEdit,canManage) VALUES(?,?,?,?,?,?,?,?);");
        statement.setString(1, journalID);
        statement.setString(2, title);
        statement.setString(3, lastUpdated);
        statement.setInt   (4, entryCount);
        statement.setString(5, ownerID);
        statement.setString(6, ownerID);
        statement.setString(7, ownerID);
        statement.setString(8, ownerID);
        statement.execute();
        connection.close();
    }

    public User getUser(UUID id) throws IOException {
        return getUser(id.toString());
    }

    public User getUser(String id) throws IOException {
        return new User(new JSONObject(Files.readString(root.resolve("users").resolve(id).resolve("data.json"))));
    }


    public User getUserByEmail(String email) throws SQLException, IOException {
        return getUserBy("email", email);
    }

    public User getUserByUsername(String username) throws SQLException, IOException {
        return getUserBy("username", username);
    }

    public User lookUpUser(String lookup) throws SQLException, IOException {
        return getUserBy("lookup", lookup);
    }

    private User getUserBy(String fieldName, String value) throws SQLException, IOException {
        Connection connection = source.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT id FROM users WHERE " + fieldName + " = ?;");
        statement.setString(1, value);
        ResultSet result = statement.executeQuery();
        if(!result.next()) return null;
        User user = getUser(result.getString(1));
        connection.close();
        return user;
    }


    public void changeEmail(User user, String newEmail) throws SQLException, IOException {
        user.setEmail(newEmail);
        user.save();
        updateProperty(user, "email", newEmail);
    }

    public void changeUsername(User user, String newUsername) throws SQLException, IOException {
        user.setUsername(newUsername);
        user.save();
        updateProperty(user, "username", newUsername);
    }

    public ArrayList<WorkspaceInfo> getWorkspaces(User user) throws SQLException {
        Connection connection = source.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM workspaces WHERE canView LIKE ?;");
        statement.setString(1, "%" + user.getID() + "%");
        ResultSet result = statement.executeQuery();
        ArrayList<WorkspaceInfo> toReturn = new ArrayList<>();
        while(result.next()) {
            toReturn.add(new WorkspaceInfo(
                UUID.fromString(result.getString(1)),
                result.getString(2),
                result.getString(4).length() > 36, // If canView is longer than 36 characters (the length of a single UUID), that means the workspace has been shared with someone else
                result.getString(3).equals(user.getID().toString())
            ));
        }
        connection.close();
        return toReturn;
    }

    public ArrayList<JournalInfo> getJournals(User user) throws SQLException {
        Connection connection = source.getConnection();
        PreparedStatement statement = connection.prepareStatement("SELECT * FROM journals WHERE canView LIKE ?;");
        statement.setString(1, "%" + user.getID() + "%");
        ResultSet result = statement.executeQuery();
        ArrayList<JournalInfo> toReturn = new ArrayList<>();
        while(result.next()) {
            toReturn.add(new JournalInfo(
                    UUID.fromString(result.getString(1)),
                    result.getString(2),
                    LocalDateTime.parse(result.getString(3)),
                    result.getInt(4)
            ));
        }
        connection.close();
        return toReturn;
    }

    private void updateProperty(User user, String propertyName, String newValue) throws SQLException {
        Connection connection = source.getConnection();
        PreparedStatement statement = connection.prepareStatement("UPDATE users SET ? = ? WHERE id = ?;");
        statement.setString(1, propertyName);
        statement.setString(2, newValue);
        statement.setString(3, user.getID().toString());
        statement.execute();
        connection.close();
    }

    public void updateJournal(String id, int entryCountOffset) throws SQLException, IOException {
        LocalDateTime lastUpdatedDate = LocalDateTime.now();
        String lastUpdated = lastUpdatedDate.toString();
        Path infoPath = root.resolve("journals").resolve(id).resolve("info.json");
        JSONObject info = new JSONObject(Files.readString(infoPath));
        info.put("lastUpdated", lastUpdated);
        info.put("lastUpdatedText", Utilities.lastUpdatedTimeFormat(lastUpdatedDate));
        int currentEntryCount = -1;
        if(entryCountOffset != 0) {
            currentEntryCount = info.getInt("entryCount");
            info.put("entryCount", currentEntryCount + entryCountOffset);
        }
        Files.writeString(infoPath, info.toString());

        Connection connection = source.getConnection();
        PreparedStatement statement;
        if(entryCountOffset != 0) {
            statement = connection.prepareStatement("UPDATE journals SET lastUpdated = ?, entryCount = ? WHERE id = ?;");
            statement.setString(1, lastUpdated);
            statement.setInt(2, currentEntryCount + entryCountOffset);
            statement.setString(3, id);
        } else {
            statement = connection.prepareStatement("UPDATE journals SET lastUpdated = ? WHERE id = ?;");
            statement.setString(1, lastUpdated);
            statement.setString(2, id);
        }
        statement.execute();
        connection.close();
    }

    public boolean canViewJournal(String journalID, String userID) throws SQLException {
        return journalHasProperty(journalID, userID, "canView");
    }

    public boolean canEditJournal(String journalID, String userID) throws SQLException {
        return journalHasProperty(journalID, userID, "canEdit");
    }

    public boolean canManageJournal(String journalID, String userID) throws SQLException {
        return journalHasProperty(journalID, userID, "canManage");
    }

    private boolean journalHasProperty(String journalID, String userID, String property) throws SQLException {
        Connection connection = source.getConnection();
        PreparedStatement checkStatement = connection.prepareStatement("SELECT id FROM journals WHERE id = ? AND " + property + " LIKE ?;");
        checkStatement.setString(1, journalID);
        checkStatement.setString(2, "%" + userID + "%");
        boolean result = checkStatement.executeQuery().next();
        connection.close();
        return result;
    }

    private static final HashMap<String, Analyzer> ANALYZER_MAP = new HashMap<>();
    static {
        ANALYZER_MAP.put("id", null);
    }

    public IndexWriter getJournalIndexWriter(String id) throws IOException {
        Path indexPath = root.resolve("journals").resolve(id).resolve("index");
        PerFieldAnalyzerWrapper analyzer = new PerFieldAnalyzerWrapper(new StandardAnalyzer(), ANALYZER_MAP);
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setCommitOnClose(true);
        return new IndexWriter(FSDirectory.open(indexPath), config);
    }

    public IndexSearcher getJournalIndexSearcher(String id) throws IOException {
        Path indexPath = root.resolve("journals").resolve(id).resolve("index");
        return new IndexSearcher(DirectoryReader.open(FSDirectory.open(indexPath)));
    }

    /**
     * @param id The {@link UUID} of the {@link Workspace} to retrieve
     * @return The workspace with the given ID, or {@code null} if it couldn't be found
     */
    public Workspace getWorkspace(UUID id) throws IOException {
        return new Workspace(new JSONObject(Files.readString(root.resolve("workspaces").resolve(id.toString()).resolve("data.json"))));
    }

    public Connection getConnection() throws SQLException {
        return source.getConnection();
    }

}
