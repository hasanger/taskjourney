package io.sanger.henry.taskjourney.api.task.duerule;

import io.sanger.henry.taskjourney.api.task.Task;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

/**
 * Describes a {@link Task} that's due every x days.
 */
public class MonthlyDueRule extends DueRule {

    private final LocalDate startDate;
    private final int interval;



    /**
     * Creates a new {@link MonthlyDueRule}.
     * @param startDate The first date for which this rule should return true
     * @param interval The number of months between each due date
     */
    public MonthlyDueRule(LocalDate startDate, int interval) {
        this.startDate = startDate;
        this.interval = interval;
    }

    /**
     * Creates a new {@link MonthlyDueRule} from a {@link JSONObject}.
     * @param object The {@link JSONObject} to use
     */
    public MonthlyDueRule(JSONObject object) {
        this.startDate = LocalDate.parse(object.getString("startDate"));
        this.interval = object.getInt("interval");
    }



    @Override
    public boolean isDue(LocalDate date) {
        // First, we calculate the number of months between the start date and the given date.
        // If the remainder when dividing this number by the interval is 0, that means the task is due on the given month.
        // Next, we check if the day of the month is the same. If it is, the task is due on the given day.
        return ChronoUnit.MONTHS.between(startDate.withDayOfMonth(1), date.withDayOfMonth(1)) % interval == 0
                && (date.isAfter(startDate) || date.isEqual(startDate))
                && date.getDayOfMonth() == startDate.getDayOfMonth();
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return date.isAfter(startDate.plusMonths(interval));
    }

    // TODO Only works for interval = 1
    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        start = start.minusDays(1);
        ArrayList<LocalDate> toReturn = new ArrayList<>();
        LocalDate date = start.withDayOfMonth(startDate.getDayOfMonth());
        if(date.isBefore(start)) date = date.plusMonths(1);
        while(date.isBefore(end)) {
            toReturn.add(date);
            date = date.plusMonths(interval);
        }
        return toReturn;
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return false;
    }

    @Override
    public boolean isDueOnce() {
        return false;
    }

    @Override
    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public String getDueString(LocalDate date) {
        return "Due " + (interval == 1 ? "every month" : "every " + interval + " months");
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "monthly");
        object.put("startDate", startDate.toString());
        object.put("interval", interval);
        return object;
    }

}
