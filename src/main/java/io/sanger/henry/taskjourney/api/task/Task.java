package io.sanger.henry.taskjourney.api.task;

import io.sanger.henry.taskjourney.api.task.priority.Priority;
import io.sanger.henry.taskjourney.api.task.duerule.AnytimeDueRule;
import io.sanger.henry.taskjourney.api.task.duerule.DueRule;
import io.sanger.henry.taskjourney.api.util.Utilities;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Represents something to be done.
 */
public class Task {

    private final UUID id;

    private String name, description;

    private final LocalDateTime timeCreated;

    private Priority priority;

    private final Set<LocalDate> completedOn, skippedOn;

    private DueRule dueRule;

    private final Set<UUID> tags;

    private final ArrayList<String> subtasks;





    /**
     * Creates a new {@link Task}. A unique ID is randomly generated.
     * @param name The name of the task
     */
    public Task(String name) {
        this.id = UUID.randomUUID();
        this.name = name;
        this.description = "";
        this.timeCreated = LocalDateTime.now();
        this.priority = Priority.UNSET;
        this.completedOn = new HashSet<>();
        this.skippedOn = new HashSet<>();
        this.dueRule = new AnytimeDueRule();
        this.tags = new HashSet<>();
        this.subtasks = new ArrayList<>();
    }

    public Task(JSONObject object) {
        this.id = UUID.fromString(object.getString("id"));
        this.name = object.getString("name");
        this.description = object.getString("description");
        this.timeCreated = LocalDateTime.parse(object.getString("timeCreated"));
        this.priority = Priority.valueOf(object.getString("priority"));
        this.completedOn = object.getJSONArray("completedOn").toList()
                .stream().map(o -> LocalDate.parse((String) o))
                .collect(Collectors.toCollection(HashSet::new));
        this.skippedOn = object.getJSONArray("skippedOn").toList()
                .stream().map(o -> LocalDate.parse((String) o))
                .collect(Collectors.toCollection(HashSet::new));
        this.dueRule = DueRule.fromJSONObject(object.getJSONObject("dueRule"));
        this.tags = Utilities.uuidArrayToHashSet(object.getJSONArray("tags"));
        this.subtasks = new ArrayList<>(object.getJSONArray("subtasks").toList().stream().map(Object::toString).toList());
    }



    /**
     * @return The {@link UUID} of this {@link Task}
     */
    public UUID getID() {
        return id;
    }

    /**
     * @return The name/title of this {@link Task}
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name/title of this {@link Task}
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return Information about this {@link Task}, in Markdown format
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description Information about this {@link Task}, in Markdown format
     */
    public void setDescription(String description) {
        this.description = description;
    }

    public Priority getPriority() {
        return priority;
    }

    public void setPriority(Priority priority) {
        this.priority = priority;
    }

    public boolean isCompleted(LocalDate date) {
        return completedOn.contains(date) || (dueRule.isDueOnce() && completedOn.size() > 0); // If the task is only due once and it's completed, it's completed regardless of date
    }

    public void setCompleted(boolean completed, LocalDate date) {
       if(completed) {
           completedOn.add(date);
           skippedOn.remove(date);
       } else {
           if(dueRule.isDueOnce()) completedOn.clear();
           else completedOn.remove(date);
       }
    }

    public boolean isSkipped(LocalDate date) {
        return skippedOn.contains(date);
    }

    public void setSkipped(boolean skipped, LocalDate date) {
        if(skipped) skippedOn.add(date);
        else skippedOn.remove(date);
    }

    /**
     * @return The names of this {@link Task}'s subtasks
     */
    public ArrayList<String> getSubtasks() {
        return subtasks;
    }

    /**
     * @return A {@link DueRule}, which describes when this {@link Task} is due
     */
    public DueRule getDueRule() {
        return dueRule;
    }

    /**
     * Sets this {@link Task}'s due rule.
     * @param dueRule A {@link DueRule}, which describes when this {@link Task} is due
     */
    public void setDueRule(DueRule dueRule) {
        this.dueRule = dueRule;
    }

    public boolean isDue(LocalDate date) {
        if(dueRule instanceof AnytimeDueRule && completedOn.size() > 0 && !completedOn.contains(date)) return false;
        //if(date.isBefore(timeCreated.toLocalDate())) return false;
        return dueRule.isDue(date);
    }

    public boolean isDueBefore(LocalDate date) {
        if(dueRule.isDueOnce() && !(completedOn.size() > 0)) return dueRule.isDueBefore(date);
        else return false;
    }

    public boolean isDueAfter(LocalDate date) {
        return dueRule.isDueAfter(date);
    }

    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("id", id);
        object.put("name", name);
        object.put("description", description);
        object.put("timeCreated", timeCreated);
        object.put("priority", priority);
        object.put("completedOn", completedOn);
        object.put("skippedOn", skippedOn);
        object.put("dueRule", dueRule.toJSONObject());
        object.put("tags", tags);
        object.put("subtasks", subtasks);
        return object;
    }

    public JSONObject toJSONObject(LocalDate date, boolean showCompleted) {
        ArrayList<String> info = new ArrayList<>();
        info.add(dueRule.getDueString(date));
        if(priority != Priority.UNSET) {
            String priorityString = "<span style=\"color: " + priority.getColor() + "\">" + priority.toHumanReadableString() + " priority</span>";
            info.add(priorityString);
        }
        JSONObject object = new JSONObject();
        object.put("id", id);
        object.put("name", name);
        object.put("description", description);
        object.put("timeCreated", timeCreated);
        object.put("priority", priority.toString());
        object.put("completed", isCompleted(date) && showCompleted);
        object.put("skipped", isSkipped(date) && showCompleted);
        object.put("dueRule", dueRule.toJSONObject());
        object.put("infoString", String.join(" | ", info));
        object.put("tags", tags);
        object.put("subtasks", subtasks);
        return object;
    }

}
