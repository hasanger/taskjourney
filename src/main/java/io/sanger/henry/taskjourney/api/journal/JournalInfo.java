package io.sanger.henry.taskjourney.api.journal;

import org.json.JSONObject;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.UUID;

public class JournalInfo {

    private final UUID id;

    private final String title;

    private final LocalDateTime lastUpdated;

    private final String lastUpdatedText;

    private final int entryCount;

    public JournalInfo(UUID id, String title, LocalDateTime lastUpdated, int entryCount) {
        this.id = id;
        this.title = title;
        this.lastUpdated = lastUpdated;
        this.lastUpdatedText = DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(lastUpdated);
        this.entryCount = entryCount;
    }

    public String getTitle() {
        return title;
    }

    public UUID getID() {
        return id;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("id", id);
        object.put("title", title);
        object.put("lastUpdated", lastUpdated);
        object.put("lastUpdatedText", lastUpdatedText);
        object.put("entryCount", entryCount);
        return object;
    }

}
