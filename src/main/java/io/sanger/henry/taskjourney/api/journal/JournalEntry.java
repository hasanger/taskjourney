package io.sanger.henry.taskjourney.api.journal;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;

import static io.sanger.henry.taskjourney.api.TaskJourneyAPI.getDatabase;

public class JournalEntry {

    private String title, contentPreview, content;

    private LocalDate date;

    private LocalDateTime created, started, finished, lastUpdated;

    private final String uniqueID, parentID;



    public JournalEntry(String title, String content, String parentID) {
        this.date = LocalDate.now();
        this.created = this.lastUpdated = LocalDateTime.now();
        this.title = title;
        this.content = content;
        updateContentPreview();
        this.uniqueID = UUID.randomUUID().toString();
        this.parentID = parentID;
    }

    public JournalEntry(String title,
                        String content,
                        LocalDate date,
                        LocalDateTime created,
                        LocalDateTime started,
                        LocalDateTime finished,
                        LocalDateTime lastUpdated,
                        String uniqueID,
                        String parentID) {
        this.title = title;
        this.content = content;
        updateContentPreview();
        this.date = date;
        this.created = created;
        this.started = started;
        this.finished = finished;
        this.lastUpdated = lastUpdated;
        this.uniqueID = uniqueID;
        this.parentID = parentID;
    }



    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public LocalDateTime getStarted() {
        return started;
    }

    public void setStarted(LocalDateTime started) {
        this.started = started;
    }

    public LocalDateTime getFinished() {
        return finished;
    }

    public void setFinished(LocalDateTime finished) {
        this.finished = finished;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public String getContent() {
        return content;
    }

    public String getContentPreview() {
        return contentPreview;
    }

    public void setContent(String content) {
        this.content = content;
        updateContentPreview();
    }

    private void updateContentPreview() {
        this.contentPreview = this.content.replace("\n", " ");
        if(this.contentPreview.length() > 100) this.contentPreview = this.contentPreview.substring(0, 100) + "...";
    }

    public void save() throws IOException {
        JSONObject entryObject = new JSONObject();
        entryObject.put("title", title);
        entryObject.put("content", content);
        entryObject.put("date", date.toString());
        entryObject.put("created", created.toString());
        entryObject.put("started", (started == null ? null : started.toString()));
        entryObject.put("finished", (finished == null ? null : finished.toString()));
        entryObject.put("lastUpdated", lastUpdated.toString());
        Files.writeString(
                getDatabase().getRoot()
                        .resolve("journals")
                        .resolve(parentID)
                        .resolve("entries")
                        .resolve(uniqueID + ".json"),
                entryObject.toString()
        );
    }

}
