package io.sanger.henry.taskjourney.api.task.workspace;

import org.json.JSONObject;

import java.util.UUID;

public class WorkspaceInfo {

    private UUID id;

    private String name;

    private boolean isShared, owner;


    public WorkspaceInfo(UUID id, String name, boolean isShared, boolean owner) {
        this.id = id;
        this.name = name;
        this.isShared = isShared;
        this.owner = owner;
    }


    public UUID getID() {
        return id;
    }

    public void setID(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isShared() {
        return isShared;
    }

    public void setShared(boolean shared) {
        isShared = shared;
    }

    public boolean isOwner() {
        return owner;
    }

    public void setOwner(boolean owner) {
        this.owner = owner;
    }

    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("id", id);
        object.put("name", name);
        object.put("shared", isShared);
        object.put("owner", owner);
        return object;
    }

}
