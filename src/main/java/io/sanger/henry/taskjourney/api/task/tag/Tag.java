package io.sanger.henry.taskjourney.api.task.tag;

import org.json.JSONObject;

import java.util.UUID;

public class Tag {

    private String name, colorCode;

    private UUID id;



    public Tag(String name, String colorCode) {
        this.name = name;
        this.colorCode = colorCode;
        this.id = UUID.randomUUID();
    }

    public Tag(String name, String colorCode, UUID id) {
        this.name = name;
        this.colorCode = colorCode;
        this.id = id;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColorCode() {
        return colorCode;
    }

    public void setColorCode(String colorCode) {
        this.colorCode = colorCode;
    }

    public UUID getID() {
        return id;
    }

    public void setID(UUID id) {
        this.id = id;
    }

    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("name", name);
        object.put("colorCode", colorCode);
        object.put("id", id);
        return object;
    }

}
