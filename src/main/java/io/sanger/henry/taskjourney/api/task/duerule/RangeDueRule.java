package io.sanger.henry.taskjourney.api.task.duerule;

import org.json.JSONObject;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;

public class RangeDueRule extends DueRule {

    private LocalDate startDate, endDate;

    public RangeDueRule(LocalDate startDate, LocalDate endDate) {
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public RangeDueRule(JSONObject object) {
        this.startDate = LocalDate.parse(object.getString("startDate"));
        this.endDate = LocalDate.parse(object.getString("endDate"));
    }

    @Override
    public boolean isDue(LocalDate date) {
        return date.isEqual(startDate) || date.isEqual(endDate) || (date.isAfter(startDate) && date.isBefore(endDate));
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return endDate.isBefore(date);
    }

    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        if(endDate.isBefore(start) || startDate.isAfter(end)) return Collections.emptyList();
        return null;
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return startDate.isAfter(date);
    }


    @Override
    public boolean isDueOnce() {
        return true;
    }

    @Override
    public LocalDate getStartDate() {
        return null;
    }

    public static final DateTimeFormatter RANGE_FORMAT = DateTimeFormatter.ofPattern("MM/dd/uuuu");

    @Override
    public String getDueString(LocalDate date) {
        return "Due from " + RANGE_FORMAT.format(startDate) + " to " + RANGE_FORMAT.format(endDate);
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "range");
        object.put("startDate", startDate.toString());
        object.put("endDate", endDate.toString());
        return object;
    }



    public LocalDate getFirstDate() {
        return startDate;
    }

    public void setFirstDate(LocalDate startDate) {
        this.startDate = startDate;
    }

    public LocalDate getSecondDate() {
        return endDate;
    }

    public void setSecondDate(LocalDate endDate) {
        this.endDate = endDate;
    }

}
