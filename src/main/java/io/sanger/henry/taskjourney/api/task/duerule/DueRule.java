package io.sanger.henry.taskjourney.api.task.duerule;

import io.sanger.henry.taskjourney.api.task.Task;
import org.json.JSONObject;

import java.time.LocalDate;
import java.util.List;

/**
 * Describes when a {@link Task} is due.
 */
public abstract class DueRule {

    /**
     * @param date The {@link LocalDate} to check
     * @return {@code true} if a {@link Task} should be due on the given date according to this {@link DueRule}, {@code false} otherwise
     */
    public abstract boolean isDue(LocalDate date);

    /**
     * @param date The {@link LocalDate} to check
     * @return {@code true} if a {@link Task} should be due before the given date according to this {@link DueRule}, {@code false} otherwise
     */
    public abstract boolean isDueBefore(LocalDate date);

    public abstract List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end);

    /**
     * @param date The {@link LocalDate} to check
     * @return {@code true} if a {@link Task} should be due after the given date according to this {@link DueRule}, {@code false} otherwise
     */
    public abstract boolean isDueAfter(LocalDate date);

    /**
     * @return Whether this {@link DueRule} describes a task that's due only once
     */
    public abstract boolean isDueOnce();

    /**
     * @return This {@link DueRule}'s start date, if applicable
     */
    public abstract LocalDate getStartDate();

    /**
     * @return A human-readable description of this {@link DueRule}
     */
    public abstract String getDueString(LocalDate date);

    /**
     * @return A {@link JSONObject} representation of this {@link DueRule}
     */
    public abstract JSONObject toJSONObject();

    public static DueRule fromJSONObject(JSONObject object) {
        return switch(object.getString("type")) {
            case "anytime" -> new AnytimeDueRule();
            case "once" -> new OnceDueRule(object);
            case "onceAtTime" -> new OnceAtTimeDueRule(object);
            case "daily" -> new DailyDueRule(object);
            case "dailyAtTime" -> new DailyAtTimeDueRule(object);
            case "weekly" -> new WeeklyDueRule(object);
            case "weeklyAtTime" -> new WeeklyAtTimeDueRule(object);
            case "monthly" -> new MonthlyDueRule(object);
            case "range" -> new RangeDueRule(object);
            default -> throw new IllegalArgumentException("Invalid DueRule type: " + object.getString("type"));
        };
    }

}
