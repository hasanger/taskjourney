package io.sanger.henry.taskjourney.api.util;

import at.favre.lib.crypto.bcrypt.BCrypt;
import at.favre.lib.crypto.bcrypt.LongPasswordStrategies;
import org.json.JSONArray;
import org.jsoup.Jsoup;
import org.ocpsoft.prettytime.PrettyTime;

import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Date;
import java.util.HashSet;
import java.util.UUID;
import java.util.logging.*;
import java.util.regex.Pattern;

public class Utilities {

    private static final Logger logger = Logger.getLogger("TaskJourney");
    static {
        // Configure the logger
        logger.setUseParentHandlers(false);
        ConsoleHandler handler = new ConsoleHandler();
        //noinspection unused
        handler.setFormatter(new Formatter() {

            // From https://stackoverflow.com/a/53211725/5905216
            // ANSI escape codes
            public static final String ANSI_RESET = "\u001B[0m";
            public static final String ANSI_BLACK = "\u001B[30m";
            public static final String ANSI_RED = "\u001B[31m";
            public static final String ANSI_GREEN = "\u001B[32m";
            public static final String ANSI_YELLOW = "\u001B[33m";
            public static final String ANSI_BLUE = "\u001B[34m";
            public static final String ANSI_PURPLE = "\u001B[35m";
            public static final String ANSI_CYAN = "\u001B[36m";
            public static final String ANSI_WHITE = "\u001B[37m";


            @Override
            public String format(LogRecord record) {
                StringBuilder builder = new StringBuilder();

                if(record.getLevel() == Level.SEVERE) {
                    builder.append(ANSI_RED);
                } else if(record.getLevel() == Level.WARNING) {
                    builder.append(ANSI_YELLOW);
                } else {
                    builder.append(ANSI_WHITE);
                }

                builder.append("[");
                builder.append(calcDate(record.getMillis()));
                builder.append("]");

                builder.append(" [");
                builder.append(record.getLevel().getName());
                builder.append("]");

                builder.append(" ");
                builder.append(record.getMessage());

                Object[] params = record.getParameters();

                if (params != null) {
                    builder.append("\t");
                    for (int i = 0; i < params.length; i++) {
                        builder.append(params[i]);
                        if (i < params.length - 1)
                            builder.append(", ");
                    }
                }

                builder.append(ANSI_RESET);
                builder.append("\n");
                return builder.toString();
            }

            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

            private String calcDate(long ms) {
                return dateFormat.format(new Date(ms));
            }
        });
        logger.addHandler(handler);
    }

    public static final SecureRandom random = new SecureRandom();

    private static final Pattern rfc2822 = Pattern.compile(
            "^[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"
    );

    static MessageDigest sha256Digest;
    static {
        try {
            sha256Digest = MessageDigest.getInstance("SHA-256");
        } catch(NoSuchAlgorithmException e) {
            ExceptionHandler.handleFatalException("Error initializing SHA-256 digest", e);
        }
    }

    public static final DateTimeFormatter TIME_FORMAT = DateTimeFormatter.ofPattern("hh:mm a");

    public static String prettyTimeFormat(LocalDate date, LocalDate reference) {
        if(date.isEqual(reference)) return "today";
        String formatted = new PrettyTime(reference).format(date);
        if(formatted.contains("hours from now") || formatted.equals("1 day from now")) return "tomorrow";
        else if(formatted.equals("1 day ago")) return "yesterday";
        else return formatted;
    }

    public static String lastUpdatedTimeFormat(LocalDateTime time) {
        return DateTimeFormatter.ofLocalizedDate(FormatStyle.SHORT).format(time);
    }

    public static String getContentPreview(String text) {
        return trim(clean(text));
    }

    public static String trim(String text) {
        if(text.length() > 100) text = text.substring(0, 100) + "...";
        return text;
    }

    public static String clean(String text) {
        return Jsoup.parse(text.replace("\n", " ")).text();
    }

    // https://stackoverflow.com/a/4895572/5905216
    public static String getSHA256Hash(byte[] input) {
        return byteArrayToHexString(sha256Digest.digest(input));
    }

    public static boolean isEmailValid(String email) {
        return rfc2822.matcher(email).matches();
    }

    public static String byteArrayToHexString(byte[] b) {
        StringBuilder result = new StringBuilder();
        for(byte value : b) {
            result.append(Integer.toString((value & 0xff) + 0x100, 16).substring(1));
        }
        return result.toString();
    }

    public static HashSet<UUID> uuidArrayToHashSet(JSONArray array) {
        return new HashSet<>(array.toList().stream().map(o -> UUID.fromString(o.toString())).toList());
    }

    public static Logger getLogger() {
        return logger;
    }

    public static String hash(String password) {
        return BCrypt.with(LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2A)).hashToString(12, password.toCharArray());
    }

    public static boolean verify(String password, String hash) {
        return BCrypt.verifyer(BCrypt.Version.VERSION_2A, LongPasswordStrategies.hashSha512(BCrypt.Version.VERSION_2A))
                     .verify(password.getBytes(StandardCharsets.UTF_8), hash.getBytes(StandardCharsets.UTF_8)).verified;
    }

}
