package io.sanger.henry.taskjourney.api.user;

import io.sanger.henry.taskjourney.api.task.workspace.Workspace;
import io.sanger.henry.taskjourney.api.util.Utilities;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.sql.SQLException;
import java.util.*;

import static io.sanger.henry.taskjourney.api.TaskJourneyAPI.getDatabase;

public class User {

    private final UUID id;

    private String email, firstName, lastName, username, passwordHash;

    private final UUID defaultWorkspace;

    private final Set<UUID> ownedWorkspaces;

    private final String rememberMeLookup;
    private final HashMap<String, Long> rememberMeRecords;



    public User(String email, String firstName, String lastName, String username, String password) throws SQLException, IOException {
        this.id = UUID.randomUUID();
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.username = username;
        this.passwordHash = Utilities.hash(password);

        Workspace defaultWorkspace = new Workspace("Default workspace", id);
        this.defaultWorkspace = defaultWorkspace.getID();
        ownedWorkspaces = new HashSet<>();
        ownedWorkspaces.add(this.defaultWorkspace);
        getDatabase().addNewWorkspace(defaultWorkspace);

        byte[] lookupBytes = new byte[9];
        Utilities.random.nextBytes(lookupBytes);
        this.rememberMeLookup = Base64.getEncoder().encodeToString(lookupBytes);
        this.rememberMeRecords = new HashMap<>();
    }

    public User(JSONObject object) {
        this.id = UUID.fromString(object.getString("id"));
        this.email = object.getString("email");
        if(object.has("firstName")) this.firstName = object.getString("firstName");
        if(object.has("lastName")) this.lastName = object.getString("lastName");
        this.username = object.getString("username");
        this.passwordHash = object.getString("passwordHash");

        this.defaultWorkspace = UUID.fromString(object.getString("defaultWorkspace"));
        this.ownedWorkspaces = Utilities.uuidArrayToHashSet(object.getJSONArray("ownedWorkspaces"));

        this.rememberMeLookup = object.getString("rememberMeLookup");
        this.rememberMeRecords = new HashMap<>();
        JSONObject rememberMeRecordsObject = object.getJSONObject("rememberMeRecords");
        for(Map.Entry<String, Object> record : rememberMeRecordsObject.toMap().entrySet()) {
            rememberMeRecords.put(record.getKey(), (long) record.getValue());
        }
    }


    public UUID getID() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public boolean changePassword(String oldPassword, String newPassword) {
        if(Utilities.verify(oldPassword, passwordHash)) {
            passwordHash = Utilities.hash(newPassword);
            return true;
        }
        else return false;
    }

    public String getRememberMeLookup() {
        return rememberMeLookup;
    }

    public UUID getDefaultWorkspace() {
        return defaultWorkspace;
    }

    public Set<UUID> getOwnedWorkspaces() {
        return ownedWorkspaces;
    }

    public HashMap<String, Long> getRememberMeRecords() {
        return rememberMeRecords;
    }

    public String generateRememberMeString() throws IOException {
        byte[] tokenBytes = new byte[24];
        Utilities.random.nextBytes(tokenBytes);
        String validator = Base64.getEncoder().encodeToString(tokenBytes);

        String validatorHash = Utilities.getSHA256Hash(validator.getBytes());
        long expires = System.currentTimeMillis() + 31557600000L; // in 1 year
        rememberMeRecords.put(validatorHash, expires);
        save();

        return rememberMeLookup + "." + validator;
    }

    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("id", id.toString());
        object.put("email", email);
        if(firstName != null) object.put("firstName", firstName);
        if(lastName != null) object.put("lastName", lastName);
        object.put("username", username);
        object.put("passwordHash", passwordHash);

        object.put("defaultWorkspace", defaultWorkspace);
        object.put("ownedWorkspaces", ownedWorkspaces);

        object.put("rememberMeLookup", rememberMeLookup);
        JSONObject rememberMeRecordsObject = new JSONObject();
        for(Map.Entry<String, Long> record : rememberMeRecords.entrySet())
            rememberMeRecordsObject.put(record.getKey(), record.getValue());
        object.put("rememberMeRecords", rememberMeRecordsObject);

        return object;
    }

    public void save() throws IOException {
        Files.writeString(getDatabase().getRoot().resolve("users").resolve(id.toString()).resolve("data.json"), toJSONObject().toString());
    }

}
