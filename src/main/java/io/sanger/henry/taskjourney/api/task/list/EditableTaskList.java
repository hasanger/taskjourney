package io.sanger.henry.taskjourney.api.task.list;

import io.sanger.henry.taskjourney.api.task.Task;
import io.sanger.henry.taskjourney.api.task.workspace.Workspace;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

/**
 * A list of {@link Task}s that can be edited. Unlike {@link Workspace}s,
 * {@link EditableTaskList}s only store the IDs of the tasks, not the tasks themselves.
 */
public class EditableTaskList extends TaskList {

    private final Set<UUID> taskIDs = new HashSet<>();

    public EditableTaskList(String name, UUID parentWorkspace) {
        super(name, parentWorkspace);
    }

    /**
     * @return The {@link UUID}s of the {@link Task}s referenced by this {@link EditableTaskList}
     */
    public Set<UUID> getTaskIDs() {
        return taskIDs;
    }

    /**
     * @return A list of the {@link Task}s referenced by this {@link EditableTaskList}
     */
    public ArrayList<Task> getTasks() {
        ArrayList<Task> list = new ArrayList<>();

        for(UUID id : taskIDs) {
            list.add(getWorkspace().getTask(id));
        }

        return list;
    }
    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "editable");
        object.put("name", getName());
        object.put("taskIDs", taskIDs);
        return object;
    }


}
