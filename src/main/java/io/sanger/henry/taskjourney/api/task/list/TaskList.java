package io.sanger.henry.taskjourney.api.task.list;

import io.sanger.henry.taskjourney.api.task.Task;
import io.sanger.henry.taskjourney.api.task.workspace.Workspace;
import org.json.JSONObject;

import java.util.UUID;

/**
 * A list of {@link Task}s. This class is never used directly.
 */
public abstract class TaskList {

    private String name;

    final UUID parentWorkspace;



    TaskList(String name, UUID parentWorkspace) {
        this.name = name;
        this.parentWorkspace = parentWorkspace;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Workspace getWorkspace() {
        return null;
        //return Database.getWorkspace(parentWorkspace);
    }

    public abstract JSONObject toJSONObject();

}
