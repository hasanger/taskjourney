package io.sanger.henry.taskjourney.api.task.duerule;

import io.sanger.henry.taskjourney.api.task.Task;
import org.json.JSONObject;

import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 * Describes a {@link Task} that's due every x days.
 */
public class DailyDueRule extends DueRule {

    private final LocalDate startDate;
    private final int interval;



    /**
     * Creates a new {@link DailyDueRule}.
     * @param startDate The first date for which this rule should return true
     * @param interval The number of days between each due date
     */
    public DailyDueRule(LocalDate startDate, int interval) {
        this.startDate = startDate;
        this.interval = interval;
    }

    /**
     * Creates a new {@link DailyDueRule} from a {@link JSONObject}.
     * @param object The {@link JSONObject} to use
     */
    public DailyDueRule(JSONObject object) {
        this.startDate = LocalDate.parse(object.getString("startDate"));
        this.interval = object.getInt("interval");
    }



    @Override
    public boolean isDue(LocalDate date) {
        // First, we calculate the number of days between the start date and the given date.
        // If the remainder when dividing this number by the interval is 0, that means the
        // number of days between the start date and the given date is divisible by the interval,
        // so the task is due on the given day.
        return Duration.between(startDate.atStartOfDay(), date.atStartOfDay()).toDays() % interval == 0 && (date.isAfter(startDate) || date.isEqual(startDate));
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return date.isAfter(startDate.plusDays(interval));
    }

    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        start = start.minusDays(1);
        ArrayList<LocalDate> toReturn = new ArrayList<>();
        LocalDate date = start.plusDays(interval - ((Duration.between(startDate.atStartOfDay(), start.atStartOfDay()).toDays() + 1) % interval));
        while(date.isBefore(end)) {
            toReturn.add(date);
            date = date.plusDays(interval);
        }
        return toReturn;
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return false;
    }

    @Override
    public boolean isDueOnce() {
        return false;
    }

    @Override
    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public String getDueString(LocalDate date) {
        return "Due " + (interval == 1 ? "every day" : "every " + interval + " days");
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "daily");
        object.put("startDate", startDate.toString());
        object.put("interval", interval);
        return object;
    }

}
