package io.sanger.henry.taskjourney.api;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.sql.SQLException;

public class TaskJourneyAPI {

    private static boolean DEBUG_MODE = true;

    private static Database database;


    /**
     * A convenience method that starts and loads everything in the TaskJourney API.
     * @param databasePath The {@link Path} to the database
     */
    public static void init(Path databasePath) throws IOException, SQLException {
        if(!Files.exists(databasePath)) Files.createDirectories(databasePath);
        database = new Database(databasePath);
    }

    public static Database getDatabase() {
        return database;
    }

    public static boolean debugModeEnabled() {
        return DEBUG_MODE;
    }

    public static void setDebugMode(boolean debugMode) {
        TaskJourneyAPI.DEBUG_MODE = debugMode;
    }

}
