package io.sanger.henry.taskjourney.api.task.duerule;

import io.sanger.henry.taskjourney.api.task.Task;
import org.json.JSONObject;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

import static io.sanger.henry.taskjourney.api.util.Utilities.TIME_FORMAT;
import static io.sanger.henry.taskjourney.api.util.Utilities.prettyTimeFormat;

/**
 * Describes a {@link Task} that's due once, by a certain date and/or time.
 */
public class OnceAtTimeDueRule extends DueRule {

    private LocalDateTime dueDateTime;

    /**
     * Creates a new {@link OnceAtTimeDueRule}.
     * @param dueDate The due date
     */
    public OnceAtTimeDueRule(LocalDate dueDate) {
        this.dueDateTime = dueDate.atStartOfDay();
    }

    public OnceAtTimeDueRule(LocalDateTime dueDateTime) {
        this.dueDateTime = dueDateTime;
    }

    public OnceAtTimeDueRule(JSONObject object) {
        this.dueDateTime = LocalDateTime.parse(object.getString("dueDateTime"));
    }

    /**
     * @return The due date, as a {@link java.time.LocalDate}.
     */
    public LocalDate getDueDate() {
        return dueDateTime.toLocalDate();
    }

    /**
     * @return The due date and time, as a {@link LocalDateTime}.
     */
    public LocalDateTime getDueDateTime() {
        return dueDateTime;
    }

    public void setDueDate(LocalDate dueDate) {
        this.dueDateTime = dueDate.atStartOfDay();
    }

    /**
     * Sets the due date and time of this {@link OnceAtTimeDueRule}.
     * @param dueDateTime The due date and time
     */
    public void setDueDateTime(LocalDateTime dueDateTime) {
        this.dueDateTime = dueDateTime;
    }

    @Override
    public boolean isDue(LocalDate date) {
        return date.isEqual(dueDateTime.toLocalDate());
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return dueDateTime.toLocalDate().isBefore(date);
    }

    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        return Collections.singletonList(dueDateTime.toLocalDate());
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return dueDateTime.toLocalDate().isAfter(date);
    }

    @Override
    public boolean isDueOnce() {
        return true;
    }

    @Override
    public LocalDate getStartDate() {
        return null;
    }

    @Override
    public String getDueString(LocalDate date) {
        return "Due " + prettyTimeFormat(dueDateTime.toLocalDate(), date) + " at " + TIME_FORMAT.format(dueDateTime);
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "onceAtTime");
        object.put("dueDateTime", dueDateTime.toString());
        return object;
    }

}

