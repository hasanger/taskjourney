package io.sanger.henry.taskjourney.api.journal;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.UUID;

public class Journal {

    private final String title, uniqueID;

    private final ArrayList<JournalEntry> entries = new ArrayList<>();

    private LocalDateTime lastUpdated;



    public Journal(String title) {
        this.title = title;
        this.uniqueID = UUID.randomUUID().toString();
        this.lastUpdated = LocalDateTime.now();
    }

    public Journal(String title, String uniqueID, LocalDateTime lastUpdated) {
        this.title = title;
        this.uniqueID = uniqueID;
        this.lastUpdated = lastUpdated;
    }



    public String getTitle() {
        return title;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public LocalDateTime getLastUpdated() {
        return lastUpdated;
    }

    public void setLastUpdated(LocalDateTime lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public ArrayList<JournalEntry> getEntries() {
        return entries;
    }

    public JournalEntry getEntry(String uniqueID) {
        for(JournalEntry entry : entries) {
            if(entry.getUniqueID().equals(uniqueID)) return entry;
        }
        return null;
    }

    public void sort() {
        entries.sort(Comparator.comparing(JournalEntry::getDate, Comparator.reverseOrder()));
    }

    public void updated() {
        lastUpdated = LocalDateTime.now();
    }

}
