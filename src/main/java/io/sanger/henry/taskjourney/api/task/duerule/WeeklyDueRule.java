package io.sanger.henry.taskjourney.api.task.duerule;

import io.sanger.henry.taskjourney.api.task.Task;
import org.json.JSONArray;
import org.json.JSONObject;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;

/**
 * Describes a {@link Task} that's due on certain day(s) every week.
 */
public class WeeklyDueRule extends DueRule {

    private final Set<DayOfWeek> dueDays;

    private final LocalDate startDate;


    /**
     * Creates a new {@link WeeklyDueRule}.
     * @param dueDays A {@link Collection} of days of the week on which this rule should return {@code true}
     * @param startDate The first {@link LocalDate} for which this rule should return {@code true}
     */
    public WeeklyDueRule(Collection<DayOfWeek> dueDays, LocalDate startDate) {
        this.dueDays = new HashSet<>(dueDays);
        this.startDate = startDate;
    }

    public WeeklyDueRule(JSONObject object) {
        this.dueDays = new HashSet<>();
        JSONArray dueDaysArray = object.getJSONArray("dueDays");
        for(int i = 0; i < dueDaysArray.length(); i++) dueDays.add(dueDaysArray.getEnum(DayOfWeek.class, i));
        this.startDate = LocalDate.parse(object.getString("startDate"));
    }


    @Override
    public boolean isDue(LocalDate date) {
        return dueDays.contains(date.getDayOfWeek()) && (date.isAfter(startDate) || date.isEqual(startDate));
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return false;
    }

    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        ArrayList<LocalDate> toReturn = new ArrayList<>();
        for(LocalDate i = start; i.isBefore(end); i = i.plusDays(1)) {
            if(dueDays.contains(i.getDayOfWeek())) toReturn.add(i);
        }
        return toReturn;
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return false;
    }

    @Override
    public boolean isDueOnce() {
        return false;
    }

    @Override
    public LocalDate getStartDate() {
        return startDate;
    }

    @Override
    public String getDueString(LocalDate date) {
        int daysCount = dueDays.size();
        if(daysCount == 0) return "Never due";
        else if(daysCount == 7) return "Due every day";
        else if(daysCount == 5 && !dueDays.contains(DayOfWeek.SATURDAY) && !dueDays.contains(DayOfWeek.SUNDAY)) return "Due on weekdays";
        else if(daysCount == 2 && dueDays.contains(DayOfWeek.SATURDAY) && dueDays.contains(DayOfWeek.SUNDAY)) return "Due on weekends";
        else if(daysCount == 6) {
            if(!dueDays.contains(DayOfWeek.MONDAY)) return "Due every day except for Mon";
            else if(!dueDays.contains(DayOfWeek.TUESDAY)) return "Due every day except for Tue";
            else if(!dueDays.contains(DayOfWeek.WEDNESDAY)) return "Due every day except for Wed";
            else if(!dueDays.contains(DayOfWeek.THURSDAY)) return "Due every day except for Thu";
            else if(!dueDays.contains(DayOfWeek.FRIDAY)) return "Due every day except for Fri";
            else if(!dueDays.contains(DayOfWeek.SATURDAY)) return "Due every day except for Sat";
            else if(!dueDays.contains(DayOfWeek.SUNDAY)) return "Due every day except for Sun";
        }

        ArrayList<String> dueList = new ArrayList<>();
        if(dueDays.contains(DayOfWeek.MONDAY)) dueList.add("Mon");
        if(dueDays.contains(DayOfWeek.TUESDAY)) dueList.add("Tue");
        if(dueDays.contains(DayOfWeek.WEDNESDAY)) dueList.add("Wed");
        if(dueDays.contains(DayOfWeek.THURSDAY)) dueList.add("Thu");
        if(dueDays.contains(DayOfWeek.FRIDAY)) dueList.add("Fri");
        if(dueDays.contains(DayOfWeek.SATURDAY)) dueList.add("Sat");
        if(dueDays.contains(DayOfWeek.SUNDAY)) dueList.add("Sun");

        if(daysCount == 1) return "Due every " + dueList.get(0);
        else if(daysCount == 2) return "Due every " + dueList.get(0) + " and " + dueList.get(1);
        else {
            String lastItem = dueList.get(dueList.size() - 1);
            dueList.remove(dueList.size() - 1);
            return "Due every " + String.join(", ", dueList) + " and " + lastItem;
        }
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "weekly");
        object.put("dueDays", dueDays);
        object.put("startDate", startDate.toString());
        return object;
    }

}
