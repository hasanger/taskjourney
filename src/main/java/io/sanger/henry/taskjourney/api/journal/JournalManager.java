package io.sanger.henry.taskjourney.api.journal;

import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.stream.Stream;

public class JournalManager {

    private static final ArrayList<Journal> journals = new ArrayList<>();


    public static ArrayList<Journal> getJournals() {
        return journals;
    }

    public static Journal getJournal(String uniqueID) {
        for(Journal journal : journals) {
            if(journal.getUniqueID().equals(uniqueID)) return journal;
        }
        return null;
    }

    public static void load() throws IOException {
        // Load journals
        try(Stream<Path> journalPaths = Files.list(Path.of("journals"))) {
            journalPaths.forEach(journalPath -> {
                try {
                    JSONObject info = new JSONObject(Files.readString(journalPath.resolve("info.json")));
                    String title = info.getString("title");
                    String uniqueID = journalPath.getFileName().toString();
                    LocalDateTime lastUpdated = LocalDateTime.parse(info.getString("lastUpdated"));
                    Journal journal = new Journal(title, uniqueID, lastUpdated);

                    try(Stream<Path> entryPaths = Files.list(journalPath.resolve("entries"))) {
                        entryPaths.forEach(entryPath -> {
                            try {
                                JSONObject entryObject = new JSONObject(Files.readString(entryPath));
                                String entryTitle = entryObject.getString("title");
                                String content = entryObject.getString("content");
                                LocalDate date = LocalDate.parse(entryObject.getString("date"));
                                LocalDateTime created = LocalDateTime.parse(entryObject.getString("created"));
                                 LocalDateTime started = null, finished = null;
                                if(entryObject.has("started")) started = LocalDateTime.parse(entryObject.getString("started"));
                                if(entryObject.has("finished")) finished = LocalDateTime.parse(entryObject.getString("finished"));
                                LocalDateTime entryLastUpdated = LocalDateTime.parse(entryObject.getString("lastUpdated"));
                                String entryUniqueID = entryPath.getFileName().toString().substring(0, 36); // The substring strips off the .json
                                JournalEntry entry = new JournalEntry(
                                        entryTitle,
                                        content,
                                        date,
                                        created,
                                        started,
                                        finished,
                                        entryLastUpdated,
                                        entryUniqueID,
                                        uniqueID
                                );
                                journal.getEntries().add(entry);
                            } catch(IOException e) {
                                throw new RuntimeException(e);
                            }
                        });
                    }

                    journal.sort();
                    journals.add(journal);
                } catch(IOException e) {
                    throw new RuntimeException(e);
                }
            });
        }
    }

}
