package io.sanger.henry.taskjourney.api.task.statistic;

import java.time.LocalDate;
import java.util.TreeMap;

public abstract class Statistic {

    private String name;

    private final TreeMap<LocalDate, Integer> values = new TreeMap<>();



    public Statistic(String name) {
        this.name = name;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void addValue(LocalDate date, int value) {
        values.put(date, value);
    }

    public TreeMap<LocalDate, Integer> getValues() {
        return values;
    }

    public int getLastValue() {
        return values.lastEntry().getValue();
    }

}
