package io.sanger.henry.taskjourney.api.task.workspace;

import io.sanger.henry.taskjourney.api.task.Task;
import io.sanger.henry.taskjourney.api.task.list.TaskList;
import io.sanger.henry.taskjourney.api.task.tag.Tag;
import io.sanger.henry.taskjourney.api.user.User;
import io.sanger.henry.taskjourney.api.util.Utilities;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import static io.sanger.henry.taskjourney.api.TaskJourneyAPI.getDatabase;

/**
 * A container for {@link Task}s. Unlike {@link TaskList}s, {@link Workspace}s store the actual tasks.
 */
public class Workspace {

    // The ID of the workspace
    private final UUID id;

    private UUID owner;

    private final Set<UUID> canView, canEdit, canManage;

    private String name;

    // List of tasks in the workspace. This is where the tasks themselves are stored
    private final ArrayList<Task> tasks = new ArrayList<>();

    private final ArrayList<Task> trash = new ArrayList<>();

    // List of task lists in the workspace
    private final ArrayList<TaskList> lists = new ArrayList<>();

    private final Set<Tag> tags = new HashSet<>();

    /**
     * Creates a new {@link Workspace}. A unique ID is randomly generated.
     * @param name The name of the workspace
     * @param owner The {@link UUID} of the user who owns the workspace
     */
    public Workspace(String name, UUID owner) {
        this.id = UUID.randomUUID();
        this.owner = owner;
        this.canView = new HashSet<>();
        this.canEdit = new HashSet<>();
        this.canManage = new HashSet<>();
        canView.add(owner);
        canEdit.add(owner);
        canManage.add(owner);
        this.name = name;
    }

    public Workspace(JSONObject object) {
        this.id = UUID.fromString(object.getString("id"));
        this.owner = UUID.fromString(object.getString("owner"));
        this.canView = Utilities.uuidArrayToHashSet(object.getJSONArray("canView"));
        this.canEdit = Utilities.uuidArrayToHashSet(object.getJSONArray("canEdit"));
        this.canManage = Utilities.uuidArrayToHashSet(object.getJSONArray("canManage"));

        this.name = object.getString("name");

        JSONArray tasksArray = object.getJSONArray("tasks");
        for(int i = 0; i < tasksArray.length(); i++) tasks.add(new Task(tasksArray.getJSONObject(i)));

        JSONArray trashArray = object.getJSONArray("trash");
        for(int i = 0; i < trashArray.length(); i++) trash.add(new Task(trashArray.getJSONObject(i)));
    }

    public UUID getID() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public UUID getOwner() {
        return owner;
    }

    public void setOwner(UUID owner) throws IOException {
        User oldOwner = getDatabase().getUser(this.owner);
        if(oldOwner != null) oldOwner.getOwnedWorkspaces().remove(id);
        this.owner = owner;
    }

    public void grantViewPermission(UUID userID) {
        canView.add(userID);
    }

    public void grantEditPermission(UUID userID) {
        canEdit.add(userID);
    }

    public void grantManagePermission(UUID userID) {
        canManage.add(userID);
    }

    public void revokeViewPermission(UUID userID) {
        canView.remove(userID);
    }

    public void revokeEditPermission(UUID userID) {
        canEdit.remove(userID);
    }

    public void revokeManagePermission(UUID userID) {
        canManage.remove(userID);
    }

    public boolean canView(User user) {
        return canView.contains(user.getID());
    }

    public boolean canEdit(User user) {
        return canEdit.contains(user.getID());
    }

    public boolean canManage(User user) {
        return canManage.contains(user.getID());
    }

    public Set<UUID> getCanView() {
        return canView;
    }

    public Set<UUID> getCanEdit() {
        return canEdit;
    }

    public Set<UUID> getCanManage() {
        return canManage;
    }

    public ArrayList<Task> getTasks() {
        return tasks;
    }

    /**
     * Gets a {@link Task} by its ID.
     * @param id The {@link UUID} of the {@link Task} to retrieve
     * @return The {@link Task} with the given ID, or {@code null} if it couldn't be found
     */
    public Task getTask(UUID id) {
        for(Task task : tasks) {
            if(task.getID().equals(id)) return task;
        }
        return null;
    }

    public void addTask(Task task) {
        this.tasks.add(task);
    }

    public void trashTask(Task task) {
        this.tasks.remove(task);
        this.trash.add(task);
    }

    public void deleteTask(Task task) {
        this.trash.remove(task);
    }

    public void restoreTask(Task task) {
        this.trash.remove(task);
        this.tasks.add(task);
    }

    public void emptyTrash() {
        this.trash.clear();
    }

    public Set<Tag> getTags() {
        return tags;
    }

    public void addTag(Tag tag) {
        this.tags.add(tag);
    }

    public ArrayList<TaskList> getLists() {
        return lists;
    }

    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("id", id.toString());
        object.put("owner", owner.toString());
        object.put("canView", canView);
        object.put("canEdit", canEdit);
        object.put("canManage", canManage);
        object.put("name", name);

        JSONArray tasksArray = new JSONArray();
        for(Task task : tasks) tasksArray.put(task.toJSONObject());
        object.put("tasks", tasksArray);

        JSONArray trashArray = new JSONArray();
        for(Task task : trash) trashArray.put(task.toJSONObject());
        object.put("trash", trashArray);

        JSONArray taskListsArray = new JSONArray();
        for(TaskList taskList : lists) taskListsArray.put(taskList.toJSONObject());
        object.put("lists", taskListsArray);

        JSONArray tagsArray = new JSONArray();
        for(Tag tag : tags) tagsArray.put(tag.toJSONObject());
        object.put("tags", tagsArray);

        return object;
    }

    public void save() throws IOException {
        Files.writeString(getDatabase().getRoot().resolve("workspaces").resolve(id.toString()).resolve("data.json"), toJSONObject().toString());
    }

}
