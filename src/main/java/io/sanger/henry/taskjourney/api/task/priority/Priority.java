package io.sanger.henry.taskjourney.api.task.priority;

import io.sanger.henry.taskjourney.api.task.Task;

/**
 * Determines the priority/urgency of a {@link Task}. Unlike difficulty, priority doesn't affect rewards.
 */
public enum Priority {
    VERY_LOW("Very low", "green"),
    LOW("Low", "#44c944"),
    MEDIUM("Medium", "#e59d16"),
    HIGH("High", "#d3d326"),
    VERY_HIGH("Very high", "#d8231a"),
    UNSET("Not set", null);

    private final String humanReadableString, color;

    Priority(String humanReadableString, String color) {
        this.humanReadableString = humanReadableString;
        this.color = color;
    }

    public String toHumanReadableString() {
        return humanReadableString;
    }

    public String getColor() {
        return color;
    }

}
