package io.sanger.henry.taskjourney.api.task.duerule;

import io.sanger.henry.taskjourney.api.task.Task;
import org.json.JSONObject;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static io.sanger.henry.taskjourney.api.util.Utilities.TIME_FORMAT;

/**
 * Describes a {@link Task} that's due every x days at a certain time.
 */
public class DailyAtTimeDueRule extends DueRule {

    private final LocalDateTime startDateTime;
    private final int interval;



    /**
     * Creates a new {@link DailyAtTimeDueRule}.
     * @param startDateTime The first date and time for which this rule should return {@code true}
     * @param interval The number of days between each due date
     */
    public DailyAtTimeDueRule(LocalDateTime startDateTime, int interval) {
        this.startDateTime = startDateTime;
        this.interval = interval;
    }

    /**
     * Creates a new {@link DailyAtTimeDueRule} from a {@link JSONObject}.
     * @param object The {@link JSONObject} to use
     */
    public DailyAtTimeDueRule(JSONObject object) {
        this.startDateTime = LocalDateTime.parse(object.getString("startDateTime"));
        this.interval = object.getInt("interval");
    }


    /**
     * @return The first date and time for which this rule returns {@code true}
     */
    public LocalDateTime getStartDateTime() {
        return startDateTime;
    }

    @Override
    public boolean isDue(LocalDate date) {
        // First, we calculate the number of days between the start date and the given date.
        // If the remainder when dividing this number by the interval is 0, that means the
        // number of days between the start date and the given date is divisible by the interval,
        // so the task is due on the given day.
        return Duration.between(startDateTime, date.atStartOfDay()).toDays() % interval == 0 && (date.isAfter(startDateTime.toLocalDate()) || date.isEqual(startDateTime.toLocalDate()));
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return date.isAfter(startDateTime.toLocalDate());
    }

    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        start = start.minusDays(1);
        ArrayList<LocalDate> toReturn = new ArrayList<>();
        LocalDate date = start.plusDays(interval - ((Duration.between(startDateTime, start.atStartOfDay()).toDays() + 1) % interval));
        while(date.isBefore(end)) {
            toReturn.add(date);
            date = date.plusDays(interval);
        }
        return toReturn;
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return false;
    }

    @Override
    public boolean isDueOnce() {
        return false;
    }

    @Override
    public LocalDate getStartDate() {
        return startDateTime.toLocalDate();
    }

    @Override
    public String getDueString(LocalDate date) {
        return "Due " + (interval == 1 ? "every day" : "every " + interval + " days") + " at " + TIME_FORMAT.format(startDateTime);
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "dailyAtTime");
        object.put("startDateTime", startDateTime.toString());
        object.put("interval", interval);
        return object;
    }

}
