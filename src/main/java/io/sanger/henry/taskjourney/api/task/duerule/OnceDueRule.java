package io.sanger.henry.taskjourney.api.task.duerule;

import io.sanger.henry.taskjourney.api.task.Task;
import io.sanger.henry.taskjourney.api.util.Utilities;
import org.json.JSONObject;

import java.time.LocalDate;
import java.util.List;

/**
 * Describes a {@link Task} that's due once, by a certain date.
 */
public class OnceDueRule extends DueRule {

    private LocalDate dueDate;

    /**
     * Creates a new {@link OnceAtTimeDueRule}.
     * @param dueDate The due date
     */
    public OnceDueRule(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    public OnceDueRule(JSONObject object) {
        this.dueDate = LocalDate.parse(object.getString("dueDate"));
    }

    /**
     * @return The due date, as a {@link LocalDate}.
     */
    public LocalDate getDueDate() {
        return dueDate;
    }

    /**
     * Sets the due date of this {@link OnceAtTimeDueRule}.
     * @param dueDate The due date
     */
    public void setDueDate(LocalDate dueDate) {
        this.dueDate = dueDate;
    }

    @Override
    public boolean isDue(LocalDate date) {
        return date.isEqual(dueDate);
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return dueDate.isBefore(date);
    }

    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        return null;
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return dueDate.isAfter(date);
    }

    @Override
    public boolean isDueOnce() {
        return true;
    }

    @Override
    public LocalDate getStartDate() {
        return null;
    }

    @Override
    public String getDueString(LocalDate date) {
        return "Due " + Utilities.prettyTimeFormat(dueDate, date);
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "once");
        object.put("dueDate", dueDate.toString());
        return object;
    }

}