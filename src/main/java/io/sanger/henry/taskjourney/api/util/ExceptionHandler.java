package io.sanger.henry.taskjourney.api.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Level;

/**
 * Handles {@link Exception}s.
 */
public class ExceptionHandler {

    /**
     * Handles an exception.
     * @param level The logging {@link Level} to use
     * @param errorMessage The error message to show
     * @param e The {@link Exception} to handle
     */
    public static void handleException(Level level, String errorMessage, Exception e) {
        Utilities.getLogger().log(level, errorMessage + ". Details: ");
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        for(String line : sw.toString().split("\n")) Utilities.getLogger().log(level, line);
    }

    /**
     * Handles an exception.
     * @param errorMessage The error message to show
     * @param e The {@link Exception} to handle
     */
    public static void handleException(String errorMessage, Exception e) {
        handleException(Level.SEVERE, errorMessage, e);
    }

    /**
     * Same as {@link ExceptionHandler#handleException(String, Exception)}, but also causes the program to exit.
     * @param errorMessage The error message to show
     * @param e The {@link Exception} to handle
     */
    public static void handleFatalException(String errorMessage, Exception e) {
        handleException(errorMessage, e);
        System.exit(1);
    }

}
