package io.sanger.henry.taskjourney.api.task.duerule;

import org.json.JSONObject;

import java.time.LocalDate;
import java.util.Collections;
import java.util.List;

public class AnytimeDueRule extends DueRule {

    @Override
    public boolean isDue(LocalDate date) {
        return true;
    }

    @Override
    public boolean isDueBefore(LocalDate date) {
        return false;
    }

    @Override
    public List<LocalDate> getDueDatesBetween(LocalDate start, LocalDate end) {
        return Collections.emptyList();
    }

    @Override
    public boolean isDueAfter(LocalDate date) {
        return false;
    }

    @Override
    public boolean isDueOnce() {
        return true;
    }

    @Override
    public LocalDate getStartDate() {
        return null;
    }

    @Override
    public String getDueString(LocalDate date) {
        return "Due anytime";
    }

    @Override
    public JSONObject toJSONObject() {
        JSONObject object = new JSONObject();
        object.put("type", "anytime");
        return object;
    }

}
