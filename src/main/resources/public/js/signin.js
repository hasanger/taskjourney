$(document).ready(function() {
	$('body').keypress(function(e) {
		if(e.key == 'Enter') {
			e.preventDefault()
			$('#signInBtn').click()
		}
	})

	$('#signInBtn').click(function() {
		$.ajax({
			url: '/api/v1/signin',
			method: 'POST',
			data: JSON.stringify({
				emailOrUsername: $('#emailOrUsernameField').val(),
				password: $('#passwordField').val(),
				rememberMe: $('#rememberMeCheckbox').prop('checked')
			}),
			success: function(data) {
				window.location = '/'
			},
			error: function(jqXHR, textStatus, errorThrown) {
				var data = JSON.parse(jqXHR.responseText)
				//console.log(data.success)
				//console.log(data.reason)
			}
		})
	})

	$('#signInLogo').click(function() {
		window.location = '/'
	})
})