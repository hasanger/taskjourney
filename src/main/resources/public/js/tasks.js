var DateTime = luxon.DateTime
var now = DateTime.now()

var colors = ['red', 'orange', 'yellow', 'green', 'blue', 'purple', 'pink']
var selectedTagColor

var searchParams = new URLSearchParams(window.location.search)
var workspaceID = searchParams.get('workspaceID')
if(workspaceID == null || workspaceID == 'null') {
    $.get('/api/v1/defaultWorkspace', function(data) {
        workspaceID = data
        searchParams.set('workspaceID', workspaceID)
        window.history.pushState(null, null, '?' + searchParams.toString())
        $(document).ready(setup)
    })
} else {
    $(document).ready(setup)
}

var taskData
var selectedTask

var shouldSave = false

var showingTaskNav = true

function toast(text) {
    const newToast = $(`
                     <div class="toast align-items-center" role="alert" aria-live="assertive" aria-atomic="true" data-bs-delay="7000">
                       <div class="d-flex">
                         <div class="toast-body">${text}</div>
                         <button type="button" class="btn-close me-2 m-auto" data-bs-dismiss="toast" aria-label="Close"></button>
                       </div>
                     </div>
                     `)
    $('.toast-container').prepend(newToast)

    const toast = new bootstrap.Toast(newToast)
    toast.show()
}

function setup() {
    const ctx = document.querySelector('#test').getContext('2d')
    Chart.defaults.color = '#cdcdcd'
    const myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: ['1/1/2022', '1/2/2022', '1/3/2022', '1/4/2022', '1/5/2022', '1/6/2022', '1/7/2022'],
            datasets: [{
                label: 'Page number',
                data: [12, 19, 26, 28, 45, 80, 150],
                backgroundColor: 'rgb(200, 45, 45)',
                borderColor: 'rgb(200, 45, 45)'
            }]
        },
        options: {
            scales: {
                x: {
                    grid: {
                        display: false
                    }
                },
                y: {
                    grid: {
                        color: '#454545',
                        borderColor: '#454545'
                    }
                }
            }
        }
    });
    const ctx2 = document.querySelector('#test2').getContext('2d')
    const myChart2 = new Chart(ctx2, {
        type: 'line',
        data: {
            labels: ['1/1/2022', '1/2/2022', '1/3/2022', '1/4/2022', '1/5/2022', '1/6/2022', '1/7/2022'],
            datasets: [{
                label: 'XP',
                data: [20, 50, 150, 80, 10, 30, 20],
                backgroundColor: 'rgb(200, 45, 45)',
                borderColor: 'rgb(200, 45, 45)'
            }]
        },
        options: {
            scales: {
                x: {
                    grid: {
                        display: false
                    }
                },
                y: {
                    grid: {
                        color: '#454545',
                        borderColor: '#454545'
                    }
                }
            }
        }
    });

    $('#syncBtn').click(sync)

    $('#dateSelectorInput').val(today().toISODate())


    $('#datePrevBtn').on('mouseup', () => changeDate($('#dateSelectorInput'), -1))
    $('#dateNextBtn').on('mouseup', () => changeDate($('#dateSelectorInput'), 1))

    $('#secondDatePrevBtn').on('mouseup', () => changeDate($('#secondDateSelectorInput'), -1))
    $('#secondDateNextBtn').on('mouseup', () => changeDate($('#secondDateSelectorInput'), 1))

    $('#dateSelectorInput').change(function(e) {
        if(!$(this).val()) {
            $('#taskDisplaySelect').val('all')
            hideSecondDate()
        } else {
            updateTaskDisplaySelect()
        }
    })

    $('#taskDisplaySelect').change(function() {
        switch($(this).val()) {
            case 'all':
                hideTaskNav()
                $('#dateSelectorInput').val('')
                hideSecondDate()
                break
            case 'yesterday':
                showTaskNav()
                $('#dateSelectorInput').val(todayPlusDays(-1).toISODate())
                hideSecondDate()
                break
            case 'today':
                showTaskNav()
                $('#dateSelectorInput').val(today().toISODate())
                hideSecondDate()
                break
            case 'tomorrow':
                showTaskNav()
                $('#dateSelectorInput').val(todayPlusDays(1).toISODate())
                hideSecondDate()
                break
            case 'fixed':
                showTaskNav()
                hideSecondDate()
                break
            case 'next7Days':
                showTaskNav()
                $('#dateSelectorInput').val(today().toISODate())
                showSecondDate()
                $('#secondDateSelectorInput').val(todayPlusDays(7).toISODate())
                break
            case 'range':
                showTaskNav()
                showSecondDate()
                if(!$('#secondDateSelectorInput').val()) $('#secondDateSelectorInput').val($('#dateSelectorInput').val())
                break
        }
        getTasks(true)
    })

    $('.nav-pills .nav-link').click(function() {
        toggleLink(this)
    })

    $('#tasksContainer').on('click', '.task', function(e) {
        e.preventDefault()

        if($(this).hasClass('task-deleting')) return

        if(shouldSave) save()

        if($(this).hasClass('task-selected')) {
            $(this).removeClass('task-selected')
            selectedTask = null
            hideTaskInfo()
        } else {
            $('.task-selected').removeClass('task-selected')
            $(this).addClass('task-selected')
            selectedTask = $(this).attr('id')

            var task = getSelectedTask()

            if(task == undefined) {
                $(this).removeClass('task-selected')
                selectedTask = null
                hideTaskInfo()
                sync()
                return
            }

            updateTaskInfo()
            showTaskInfo()
        }
    })

    $('#tasksContainer').on('keydown', '#newTaskNameField', function(e) {
        if(e.key == 'Enter') {
            $('#newTaskBtn').click()
        }
    })

    $('#tasksContainer').on('click', '#newTaskBtn', function() {
        var taskName = $('#newTaskNameField').val()
        $('#newTaskNameField').val('')
        $.ajax({
            type: 'POST',
            url: `/api/v1/workspace/${workspaceID}/edit/newTask`,
            data: JSON.stringify({
                name: taskName
            }),
            dataType: 'json',
            success: function(task) {
                var newTask = $(`
                                <div id="${task.id}" class="task list-group-item d-flex gap-3" style="display: none">
                                  <input class="task-checkbox form-check-input flex-shrink-0" type="checkbox">
                                    <span class="pt-1 form-checked-content">
                                      <strong class="task-title">${task.name}</strong>
                                      <div class="task-tag-container"></div>
                                      <span class="task-description"></span>
                                      <small class="d-block text-muted">
                                        Due anytime
                                      </small>
                                  </span>
                                </div>
                                `)
                taskData.tasks.push(task)
                $('.all-done-item').remove()
                $('#toDoList').append(newTask)
                newTask.fadeIn(500)
            }
        })
    })

    $('#tasksContainer').on('click', '.task-checkbox', function(e) {
        e.stopPropagation()

        var checkbox = $(this)
        var task = $(this).parent()
        var taskID = task.attr('id')

        var isChecked = checkbox.is(':checked')

        if(isChecked) {
            party.confetti(this)
            playSound('/sfx/completed.wav')
            task.addClass('task-completed')
            getTask(taskID).completed = true
            if(taskID == selectedTask) $('#bigTaskCheckbox').prop('checked', true)
            $.ajax({
                type: 'POST',
                url: `/api/v1/workspace/${workspaceID}/edit/task/${taskID}/complete`,
                data: $('#dateSelectorInput').val()
            })
        } else {
            if(getTask(taskID).completed) {
                playSound('/sfx/uncompleted.wav')
                task.removeClass('task-completed')
                getTask(taskID).completed = false
                if(taskID == selectedTask) {
                    $('#bigTaskCheckbox').prop('checked', false)
                    if(taskObj.skipped) $('#bigTaskCheckbox').removeClass('skipped')
                }
                $.ajax({
                    type: 'POST',
                    url: `/api/v1/workspace/${workspaceID}/edit/task/${task.attr('id')}/uncomplete`,
                    data: $('#dateSelectorInput').val()
                })
            } else if(getTask(taskID).skipped) {
                task.removeClass('task-completed')
                getTask(taskID).skipped = false
                if(taskID == selectedTask) {
                    $('#bigTaskCheckbox').prop('checked', false)
                    $('#bigTaskCheckbox').removeClass('skipped')
                }
                $('#taskSkipBtn').prop('disabled', false)
                bootstrap.Tooltip.getInstance('#taskSkipBtnTooltipWrapper')
                                 .setContent({'.tooltip-inner': 'Skip'})
                $.ajax({
                    type: 'POST',
                    url: `/api/v1/workspace/${workspaceID}/edit/task/${task.attr('id')}/unskip`,
                    data: $('#dateSelectorInput').val()
                })
            }
        }
    })

    $('#bigTaskCheckbox').click(function() {
        var task = $(`#${selectedTask}`)
        var taskID = task.attr('id')

        var isChecked = $(this).is(':checked')

        if(isChecked) {
            party.confetti(this)
            playSound('/sfx/completed.wav')
            task.addClass('task-completed')
            getTask(taskID).completed = true
            task.find('.task-checkbox').prop('checked', true)
            $.ajax({
                type: 'POST',
                url: `/api/v1/workspace/${workspaceID}/edit/task/${taskID}/complete`,
                data: $('#dateSelectorInput').val()
            })
        } else {
            if(getTask(taskID).completed) {
                playSound('/sfx/uncompleted.wav')
                task.removeClass('task-completed')
                getTask(taskID).completed = false
                task.find('.task-checkbox').prop('checked', false)
                $.ajax({
                    type: 'POST',
                    url: `/api/v1/workspace/${workspaceID}/edit/task/${task.attr('id')}/uncomplete`,
                    data: $('#dateSelectorInput').val()
                })
            } else if(getTask(taskID).skipped) {
                task.removeClass('task-completed')
                task.find('.task-checkbox').prop('checked', false)
                task.find('.task-checkbox').removeClass('skipped')
                $(this).removeClass('skipped')
                getTask(taskID).skipped = false
                $('#taskSkipBtn').prop('disabled', false)
                bootstrap.Tooltip.getInstance('#taskSkipBtnTooltipWrapper')
                                 .setContent({'.tooltip-inner': 'Skip'})
                $.ajax({
                    type: 'POST',
                    url: `/api/v1/workspace/${workspaceID}/edit/task/${task.attr('id')}/unskip`,
                    data: $('#dateSelectorInput').val()
                })
            }
        }
    })

    $(`
      #taskNameField,
      #taskDescriptionField,
      #intervalInput,
      #startDateInput,
      #timeInput,
      #onceDateInput,
      #daysOfWeekContainer input,
      #firstRangeInput,
      #secondRangeInput
      `).on('input', () => shouldSave = true)
    $('#dueSelect').change(function() {
        updateDueContainer(true)
        shouldSave = true
        if($('#startDateContainer').is(':visible')) $('#startDateInput').val(today().toISODate())
    })

    $('#priorityContainer .dropdown-item').click(function() {
        shouldSave = true
        var task = getSelectedTask()
        var priority, priorityText, textColor

        switch($(this).attr('id')) {
            case 'notSetOption':
                priority = 'UNSET'
                priorityText = 'Not set '
                textColor = 'initial'
                break
            case 'veryLowOption':
                priority = 'VERY_LOW'
                priorityText = 'Very low '
                textColor = 'green'
                break
            case 'lowOption':
                priority = 'LOW'
                priorityText = 'Low '
                textColor = '#44c944'
                break
            case 'mediumOption':
                priority = 'MEDIUM'
                priorityText = 'Medium '
                textColor = '#e59d16'
                break
            case 'highOption':
                priority = 'HIGH'
                priorityText = 'High '
                textColor = '#d3d326'
                break
            case 'veryHighOption':
                priority = 'VERY_HIGH'
                priorityText = 'Very high '
                textColor = '#d8231a'
                break
        }
        task.priority = priority
        $('#prioritySelectBtn').attr('data-value', priority)
        $('#prioritySelectBtn').text(priorityText)
        $('#prioritySelectBtn').css('color', textColor)
    })

    $('#intervalInput').change(updateIntervalText)

    setInterval(function() {
        if(shouldSave) save()
    }, 500)

    $('#taskSkipBtn').click(function() {
        shouldSave = true
        const task = getSelectedTask()
        task.skipped = true
        $(this).prop('disabled', true)
        bootstrap.Tooltip.getInstance('#taskSkipBtnTooltipWrapper')
                         .setContent({'.tooltip-inner': 'Already skipped. To unskip, click on the checkbox.'})
        const taskElem = $(`#${selectedTask}`)
        const checkbox = taskElem.find('.task-checkbox')
        taskElem.addClass('task-completed')
        checkbox.prop('checked', true)
        checkbox.addClass('skipped')
        $('#bigTaskCheckbox').prop('checked', true)
        $('#bigTaskCheckbox').addClass('skipped')
        $.ajax({
            type: 'POST',
            url: `/api/v1/workspace/${workspaceID}/edit/task/${selectedTask}/skip`,
            data: $('#dateSelectorInput').val()
        })
    })

    $('#taskTrashBtn').click(function() {
        var actualSelectedTask = selectedTask
        var task = $(`#${actualSelectedTask}`)
        //var choice = confirm(`Are you sure you want to delete this task?\n\n"${task.find('.task-title').text()}" will be lost forever! (A long time!)`)
        //if(choice) {
        $.ajax({
            type: 'DELETE',
            url: `/api/v1/workspace/${workspaceID}/edit/task/${selectedTask}/trash`,
            success: function() {
                task.addClass('task-deleting')
                selectedTask = null
                hideTaskInfo()
                task.fadeOut(500, function() {
                    var i = 0
                    for(const task2 of taskData.tasks) {
                        if(task2.id == task.attr('id')) {
                            taskData.tasks.splice(i, 1)
                            break
                        }
                        i++
                    }
                    task.remove()
                })
            }
        })
        //}
    })

    $('input[name="tagColor"]').click(function() {
        for(color of colors) {
            var elem = $('label[for="' + color + 'Option"] .bi')
            if($(this).attr('id').includes(color)) elem.css('visibility', 'visible')
            else elem.css('visibility', 'hidden')
        }

        var label = $('label[for="customOption"]')
        label.css('background', 'linear-gradient(to top left, #ff0000, #0000ff)')
        var icon = label.find('.bi')
        icon.removeClass('bi-check2').addClass('bi-plus-lg')
        icon.css('color', 'white')
    })

    $('#customOption').change(function() {
        var label = $('label[for="customOption"]')
        label.css('background', $(this).val())
        var icon = label.find('.bi')
        icon.removeClass('bi-plus-lg').addClass('bi-check2')
        icon.css('color', invertColor($(this).val(), true))
        for(color of colors) {
            $('label[for="' + color + 'Option"] .bi').css('visibility', 'hidden')
        }
    })

    $('#logoContainer').click(() => window.location = '/')

    $('.toast-container').on('hidden.bs.toast', '.toast', function() {
        $(this).remove()
    })

    $(`
      #taskSkipBtnTooltipWrapper,
      #taskUpBtn,
      #taskDownBtn,
      #taskToTopBtn,
      #taskToBottomBtn,
      #taskDuplicateBtn,
      #taskTrashBtn
      `).each(function() {
        new bootstrap.Tooltip(this)
    })

    $.get('/api/v1/whoami', function(data) {
        if(data.fullName) {
            $('.full-name-header').text(data.fullName)
        } else {
            $('.full-name-header').hide()
            $('.username-span').addClass('no-full-name')
        }
        $('.username-span').text('@' + data.username)
    })

    $.get('/api/v1/workspaces', function(workspaces) {
        for(workspace of workspaces) {
            var newWorkspace = $(`
                                 <li class="list-group-item position-relative${workspace.id == workspaceID ? ' active' : ''}">
                                   <a href="/tasks?workspaceID=${workspace.id}">${workspace.name}</a>
                                   ${workspace.shared ? '<i class="bi bi-share-fill"></i>' : ''}
                                   ${workspace.owner ? '<span class="badge owner-badge">Owner</span>' : ''}
                                 </li>
                                 `)
            $('.workspaces-group').append(newWorkspace)
        }
    })

    $.get(`/api/v1/workspace/${workspaceID}/lists`, function(lists) {
        for(const list of lists) {
            var newList = $(`
                            <li class="list-group-item">
                              <a href="/tasks?workspaceID=${workspaceID}&listID=whatever">${list.name}</a>
                            </li>
                            `)
            $('.lists-group').append(newList)
        }
        $('.lists-group').append($(`
                                   <li class="list-group-item">
                                     <a href="/tasks?workspaceID=${workspaceID}"><i class="bi bi-list-ul"></i> All</a>
                                   </li>
                                   <li class="list-group-item">
                                     <a href="/tasks?workspaceID=${workspaceID}&listID=completed"><i class="bi bi-check2-square"></i> Completed</a>
                                   </li>
                                   <li class="list-group-item">
                                     <a href="/tasks?workspaceID=${workspaceID}&listID=archive"><i class="bi bi-archive"></i> Archive</a>
                                   </li>
                                   <li class="list-group-item">
                                     <a href="/tasks?workspaceID=${workspaceID}&listID=trash"><i class="bi bi-trash3-fill"></i> Trash</a>
                                   </li>
                                   `))
    })

    getTasks(false)
}

function save() {
    shouldSave = false
    var task = $(`#${selectedTask}`)
    var taskInfo = {}
    var oldTaskInfo = getSelectedTask()

    taskInfo.name = oldTaskInfo.name = $('#taskNameField').val()
    taskInfo.description = oldTaskInfo.description = $('#taskDescriptionField').val()
    taskInfo.priority = oldTaskInfo.priority = $('#prioritySelectBtn').attr('data-value')


    var dueRule = {}
    dueRule.type = $('#dueSelect').val()
    switch($('#dueSelect').val()) {
        case 'anytime':
            break
        case 'once':
            dueRule.dueDate = $('#onceDateInput').val()
            break
        case 'onceAtTime':
            dueRule.dueDateTime = `${$('#onceDateInput').val()}T${$('#timeInput').val()}`
            break
        case 'daily':
        case 'monthly':
        case 'yearly':
            dueRule.startDate = $('#startDateInput').val()
            dueRule.interval = parseInt($('#intervalInput').val())
            break
        case 'dailyAtTime':
            dueRule.startDateTime = `${$('#startDateInput').val()}T${$('#timeInput').val()}`
            dueRule.interval = parseInt($('#intervalInput').val())
            break
        case 'weekly':
            dueRule.startDate = $('#startDateInput').val()
            var dueDays = []
            if($('#mondayOption').is(':checked')) dueDays.push('MONDAY')
            if($('#tuesdayOption').is(':checked')) dueDays.push('TUESDAY')
            if($('#wednesdayOption').is(':checked')) dueDays.push('WEDNESDAY')
            if($('#thursdayOption').is(':checked')) dueDays.push('THURSDAY')
            if($('#fridayOption').is(':checked')) dueDays.push('FRIDAY')
            if($('#saturdayOption').is(':checked')) dueDays.push('SATURDAY')
            if($('#sundayOption').is(':checked')) dueDays.push('SUNDAY')
            dueRule.dueDays = dueDays
            break
        case 'weeklyAtTime':
            dueRule.startDateTime = `${$('#startDateInput').val()}T${$('#timeInput').val()}`
            var dueDays = []
            if($('#mondayOption').is(':checked')) dueDays.push('MONDAY')
            if($('#tuesdayOption').is(':checked')) dueDays.push('TUESDAY')
            if($('#wednesdayOption').is(':checked')) dueDays.push('WEDNESDAY')
            if($('#thursdayOption').is(':checked')) dueDays.push('THURSDAY')
            if($('#fridayOption').is(':checked')) dueDays.push('FRIDAY')
            if($('#saturdayOption').is(':checked')) dueDays.push('SATURDAY')
            if($('#sundayOption').is(':checked')) dueDays.push('SUNDAY')
            dueRule.dueDays = dueDays
            break
        case 'range':
            dueRule.startDate = $('#firstRangeInput').val()
            dueRule.endDate = $('#secondRangeInput').val()
            break
    }
    taskInfo.dueRule = oldTaskInfo.dueRule = dueRule

    task.find('.task-title').text(taskInfo.name)
    task.find('.task-description').html(marked.parse(taskInfo.description))

    $.ajax({
        type: 'POST',
        url: `/api/v1/workspace/${workspaceID}/edit/task/${selectedTask}/`,
        data: JSON.stringify(taskInfo),
        success: () => getTasks(true, false)
    })
}

function showTaskNav() {
    if(!($('.nav-pills').is(':visible') && showingTaskNav)) {
        $('.nav-pills').fadeIn(1000)
        showingTaskNav = true
    }
}

function hideTaskNav() {
    if($('.nav-pills').is(':visible')) {
        $('.nav-pills').fadeOut(1000)
        showingTaskNav = false
    }
}

function showTaskInfo() {
    $('#nothingSelectedSpan').hide()
    $('#taskInfoContainer').show()
}

function hideTaskInfo() {
    $('#nothingSelectedSpan').show()
    $('#taskInfoContainer').hide()
}

function updateTaskInfo() {
    var task = getTask(selectedTask)

    if(task.completed || task.skipped) $('#bigTaskCheckbox').prop('checked', true)
    else $('#bigTaskCheckbox').prop('checked', false)

    $('#taskSkipBtn').prop('disabled', task.skipped)

    if(task.skipped) {
        $('#bigTaskCheckbox').addClass('skipped')
        bootstrap.Tooltip.getInstance('#taskSkipBtnTooltipWrapper')
                         .setContent({'.tooltip-inner': 'Already skipped. To unskip, click on the checkbox.'})
    } else {
        $('#bigTaskCheckbox').removeClass('skipped')
        bootstrap.Tooltip.getInstance('#taskSkipBtnTooltipWrapper')
                         .setContent({'.tooltip-inner': 'Skip'})
    }

    // Prevent completing task if displaying all tasks
    $('#bigTaskCheckbox').prop('disabled', $('#taskDisplaySelect').val() == 'all')

    $('#taskNameField').val(task.name)
    $('#taskDescriptionField').val(task.description)

    $('#dueSelect').val(task.dueRule.type)
    switch(task.dueRule.type) {
        case 'daily':
        case 'monthly':
        case 'yearly':
            $('#startDateInput').val(task.dueRule.startDate)
            $('#intervalInput').val(task.dueRule.interval)
            updateIntervalText()
            break
        case 'dailyAtTime':
            $('#intervalInput').val(task.dueRule.interval)
            updateIntervalText()
            $('#timeInput').val(task.dueRule.startDateTime.slice(-5))
            $('#startDateInput').val(task.dueRule.startDateTime.slice(0, 10))
            break
        case 'once':
            $('#onceDateInput').val(task.dueRule.dueDate)
            break
        case 'onceAtTime':
            $('#timeInput').val(task.dueRule.dueDateTime.slice(-5))
            $('#onceDateInput').val(task.dueRule.dueDateTime.slice(0, 10))
            break
        case 'weekly':
            var dueDays = task.dueRule.dueDays
            if(dueDays.includes('MONDAY')) $('#mondayOption').prop('checked', true)
            else $('#mondayOption').prop('checked', false)

            if(dueDays.includes('TUESDAY')) $('#tuesdayOption').prop('checked', true)
            else $('#tuesdayOption').prop('checked', false)
            
            if(dueDays.includes('WEDNESDAY')) $('#wednesdayOption').prop('checked', true)
            else $('#wednesdayOption').prop('checked', false)
            
            if(dueDays.includes('THURSDAY')) $('#thursdayOption').prop('checked', true)
            else $('#thursdayOption').prop('checked', false)
            
            if(dueDays.includes('FRIDAY')) $('#fridayOption').prop('checked', true)
            else $('#fridayOption').prop('checked', false)
            
            if(dueDays.includes('SATURDAY')) $('#saturdayOption').prop('checked', true)
            else $('#saturdayOption').prop('checked', false)
            
            if(dueDays.includes('SUNDAY')) $('#sundayOption').prop('checked', true)
            else $('#sundayOption').prop('checked', false)
            
            $('#startDateInput').val(task.dueRule.startDate)
            break
        case 'weeklyAtTime':
            var dueDays = task.dueRule.dueDays
            if(dueDays.includes('MONDAY')) $('#mondayOption').prop('checked', true)
            else $('#mondayOption').prop('checked', false)

            if(dueDays.includes('TUESDAY')) $('#tuesdayOption').prop('checked', true)
            else $('#tuesdayOption').prop('checked', false)
            
            if(dueDays.includes('WEDNESDAY')) $('#wednesdayOption').prop('checked', true)
            else $('#wednesdayOption').prop('checked', false)
            
            if(dueDays.includes('THURSDAY')) $('#thursdayOption').prop('checked', true)
            else $('#thursdayOption').prop('checked', false)
            
            if(dueDays.includes('FRIDAY')) $('#fridayOption').prop('checked', true)
            else $('#fridayOption').prop('checked', false)
            
            if(dueDays.includes('SATURDAY')) $('#saturdayOption').prop('checked', true)
            else $('#saturdayOption').prop('checked', false)
            
            if(dueDays.includes('SUNDAY')) $('#sundayOption').prop('checked', true)
            else $('#sundayOption').prop('checked', false)
            
            $('#timeInput').val(task.dueRule.startDateTime.slice(-5))
            $('#startDateInput').val(task.dueRule.startDateTime.slice(0, 10))
            break
    }
    updateDueContainer()

    setPriority(task.priority)
}

function updateDueContainer(reset) {
    switch($('#dueSelect').val()) {
        case 'anytime':
            $('#onceContainer').hide()
            $('#intervalContainer').hide()
            $('#timeContainer').hide()
            $('#daysOfWeekContainer').hide()
            $('#rangeContainer').hide()
            $('#startDateContainer').hide()
            break

        case 'daily':
        case 'monthly':
        case 'yearly':
            $('#onceContainer').hide()
            $('#intervalContainer').show()
            $('#timeContainer').hide()
            $('#daysOfWeekContainer').hide()
            $('#rangeContainer').hide()
            $('#startDateContainer').show()
            updateIntervalText()
            if(reset) $('#startDateInput').val(today().toISODate())
            break

        case 'dailyAtTime':
            $('#onceContainer').hide()
            $('#intervalContainer').show()
            $('#timeContainer').show()
            $('#daysOfWeekContainer').hide()
            $('#rangeContainer').hide()
            $('#startDateContainer').show()
            if(reset) $('#timeInput').val(today().toLocaleString(DateTime.TIME_24_SIMPLE))
            break

        case 'once':
            $('#onceContainer').show()
            $('#intervalContainer').hide()
            $('#timeContainer').hide()
            $('#daysOfWeekContainer').hide()
            $('#rangeContainer').hide()
            $('#startDateContainer').hide()
            if(reset) $('#onceDateInput').val(today().toISODate())
            break

        case 'onceAtTime':
            $('#onceContainer').show()
            $('#intervalContainer').hide()
            $('#timeContainer').show()
            $('#daysOfWeekContainer').hide()
            $('#rangeContainer').hide()
            $('#startDateContainer').hide()
            if(reset) $('#onceDateInput').val(today().toISODate())
            if(reset) $('#timeInput').val(today().toLocaleString(DateTime.TIME_24_SIMPLE))
            break

        case 'weekly':
            $('#onceContainer').hide()
            $('#intervalContainer').hide()
            $('#timeContainer').hide()
            $('#daysOfWeekContainer').show()
            $('#rangeContainer').hide()
            $('#startDateContainer').show()
            if(reset) {
                $('#mondayOption').prop('checked', true)
                $('#tuesdayOption').prop('checked', true)
                $('#wednesdayOption').prop('checked', true)
                $('#thursdayOption').prop('checked', true)
                $('#fridayOption').prop('checked', true)
                $('#saturdayOption').prop('checked', true)
                $('#sundayOption').prop('checked', true)
            }
            break

        case 'weeklyAtTime':
            $('#onceContainer').hide()
            $('#intervalContainer').hide()
            $('#timeContainer').show()
            $('#daysOfWeekContainer').show()
            $('#rangeContainer').hide()
            $('#startDateContainer').show()
            if(reset) {
                $('#timeInput').val(today().toLocaleString(DateTime.TIME_24_SIMPLE))
                $('#mondayOption').prop('checked', true)
                $('#tuesdayOption').prop('checked', true)
                $('#wednesdayOption').prop('checked', true)
                $('#thursdayOption').prop('checked', true)
                $('#fridayOption').prop('checked', true)
                $('#saturdayOption').prop('checked', true)
                $('#sundayOption').prop('checked', true)
            }
            break

        case 'range':
            $('#onceContainer').hide()
            $('#intervalContainer').hide()
            $('#timeContainer').hide()
            $('#daysOfWeekContainer').hide()
            $('#rangeContainer').show()
            $('#startDateContainer').hide()
            if(reset) $('#firstRangeInput, #secondRangeInput').val(today().toISODate())
            break
    }
}

function setPriority(priority) {
    var task = getSelectedTask()
    var priorityText, textColor
    switch(priority) {
        case 'UNSET':
            priorityText = 'Not set '
            textColor = 'initial'
            break
        case 'VERY_LOW':
            priorityText = 'Very low '
            textColor = 'green'
            break
        case 'LOW':
            priorityText = 'Low '
            textColor = '#44c944'
            break
        case 'MEDIUM':
            priorityText = 'Medium '
            textColor = '#e59d16'
            break
        case 'HIGH':
            priorityText = 'High '
            textColor = '#d3d326'
            break
        case 'VERY_HIGH':
            priorityText = 'Very high '
            textColor = '#d8231a'
            break
    }
    task.priority = priority
    $('#prioritySelectBtn').attr('data-value', priority)
    $('#prioritySelectBtn').text(priorityText)
    $('#prioritySelectBtn').css('color', textColor)
}

function sync() {
    getTasks(true)
}

function getTasks(clearLists, shouldUpdateTaskInfo, ignoreTaskCheck) {

    var requestData = {}

    switch($('#taskDisplaySelect').val()) {
        case 'all':
            requestData.return = 'all'
            break

        case 'yesterday':
        case 'today':
        case 'tomorrow':
        case 'fixed':
            requestData.return = 'forDate'
            requestData.date = $('#dateSelectorInput').val()
            break

        case 'next7Days':
        case 'range':
            requestData.return = 'forRange'
            requestData.firstDate = $('#dateSelectorInput').val()
            requestData.secondDate = $('#secondDateSelectorInput').val()
            break
    }

    if($('#pastDueLink').hasClass('active')) requestData.due = 'pastDue'
    else if($('#nowLink').hasClass('active')) requestData.due = 'now'
    else if($('#laterLink').hasClass('active')) requestData.due = 'later'

    $.ajax({
        type: 'GET',
        url: `/api/v1/workspace/${workspaceID}/tasks`,
        data: requestData,
        dataType: 'json',
        success: function(data) {
            taskData = data

            if(data.pastDueCount == 0 && data.nowCount == 0 && data.laterCount == 0) {
                $('#toDoList').html(`
                                    <div id="newTaskItem" class="list-group-item d-flex gap-3">
                                      <input id="newTaskBox" class="task-checkbox form-check-input flex-shrink-0" type="checkbox" disabled>
                                      <span id="newTaskInputContainer" class="pt-1 w-100 d-flex gap-3 justify-content-between" style="margin-top: -2px">
                                        <input id="newTaskNameField" class="form-control flex-grow-1" type="text" placeholder="Task name">
                                        <button id="newTaskBtn" class="btn btn-primary graybtn" type="button"><i class="bi bi-plus-circle"></i> New task</button>
                                      </span>
                                    </div>
                                    <div class="all-done-item list-group-item">No tasks.</div>
                                    `)
                return
            }

            if(!ignoreTaskCheck) {
                if(data.pastDueCount == 0) {
                    $('#pastDueLink').parent().hide()
                } else {
                    if(data.nowCount == 0 && data.laterCount == 0 && !$('#pastDueLink').hasClass('active')) {
                        toggleLink('#pastDueLink')
                    }
                    $('#pastDueLink').parent().show()
                    $('#pastDueLink .badge').text(data.pastDueCount)
                }

                if(data.nowCount == 0) {
                    $('#nowLink').parent().hide()
                } else {
                    if(data.pastDueCount == 0 && data.laterCount == 0 && !$('#nowLink').hasClass('active')) {
                        toggleLink('#nowLink')
                    }
                    $('#nowLink').parent().show()
                    $('#nowLink .badge').text(data.nowCount)
                }

                if(data.laterCount == 0) {
                    $('#laterLink').parent().hide()
                } else {
                    if(data.pastDueCount == 0 && data.nowCount == 0 && !$('#laterLink').hasClass('active')) {
                        toggleLink('#laterLink')
                    }
                    $('#laterLink').parent().show()
                    $('#laterLink .badge').text(data.laterCount)
                }
            }

            $('#taskNav').show()
            showingTaskNav = true

            if(clearLists) {
                if($('#nowLink').hasClass('active')) {
                    $('#toDoList').html(`
                                        <div id="newTaskItem" class="list-group-item d-flex gap-3">
                                          <input id="newTaskBox" class="task-checkbox form-check-input flex-shrink-0" type="checkbox" disabled>
                                          <span id="newTaskInputContainer" class="pt-1 w-100 d-flex gap-3 justify-content-between" style="margin-top: -2px">
                                            <input id="newTaskNameField" class="form-control flex-grow-1" type="text" placeholder="Task name">
                                            <button id="newTaskBtn" class="btn btn-primary graybtn" type="button"><i class="bi bi-plus-circle"></i> New task</button>
                                          </span>
                                        </div>
                                        `)
                } else {
                    $('#toDoList').html('')
                }
                $('#skippedList').html('')
                $('#completedList').html('')
            }

            for(task of data.tasks) {
                var newTask = $(`
                                <div id="${task.id}" class="task${(task.completed || task.skipped) ? ' task-completed' : ''} list-group-item d-flex gap-3">
                                  <input class="task-checkbox${task.skipped ? ' skipped' : ''} form-check-input flex-shrink-0" type="checkbox"${(task.completed || task.skipped) ? ' checked' : ''}${requestData.return == 'all' ? ' disabled' : ''}>
                                    <span class="pt-1 form-checked-content">
                                      <strong class="task-title">${task.name}</strong>
                                      <div class="task-tag-container"></div>
                                      <span class="task-description">${marked.parse(task.description)}</span>
                                      <small class="d-block text-muted">
                                        ${task.infoString}
                                      </small>
                                  </span>
                                </div>
                                `)
                $('#toDoList').append(newTask)
                /*if(task.completed) {
                    $('#completedList').append(newTask)
                } else {
                    if(task.skipped) $('#skippedList').append(newTask)
                    else $('#toDoList').append(newTask)
                }*/
            }

            if(!$(`#${selectedTask}`).length) {
                if(shouldSave) save()
                selectedTask = null
                hideTaskInfo()
            } else {
                $(`#${selectedTask}`).addClass('task-selected')
                if(shouldUpdateTaskInfo) updateTaskInfo()
            }

            /*if(!$('#toDoList').html()) {
                $('#toDoListHeader').hide()
                $('#addTaskCollapseBtn').hide()
                bootstrap.Collapse.getInstance('#newTaskCollapse').hide()
            } else {
                $('#toDoListHeader').show()
                $('#addTaskCollapseBtn').show()
            }

            if(!$('#skippedList').html()) $('#skippedListHeader').hide()
            else $('#skippedListHeader').show()

            if(!$('#completedList').html()) $('#completedListHeader').hide()
            else $('#completedListHeader').show()*/
        }
    })

    if(!$('#tasksContainer').is(':visible')) $('#tasksContainer').show()
}

function getTask(id) {
    for(task of taskData.tasks) {
        if(task.id == id) return task
    }
}

function getSelectedTask() {
    return getTask(selectedTask)
}

function changeDate(elem, amount) {
    if(elem.val()) {
        var date = DateTime.fromISO(elem.val()).plus({ days: amount })
        elem.val(date.toISODate())
        updateTaskDisplaySelect()
    }
}

function updateTaskDisplaySelect() {
    if(!$('#dateSelectorInput').val()) $('#taskDisplaySelect').val('all')

    var date = DateTime.fromISO($('#dateSelectorInput').val())
    if($('#secondDateSelectorInput').val()) {
        var date2 = DateTime.fromISO($('#secondDateSelectorInput').val())
        if(isToday(date) && areEqual(date2, todayPlusDays(7))) $('#taskDisplaySelect').val('next7Days')
        else $('#taskDisplaySelect').val('range')
    } else {
        if(isYesterday(date)) $('#taskDisplaySelect').val('yesterday')
        else if(isToday(date)) $('#taskDisplaySelect').val('today')
        else if(isTomorrow(date)) $('#taskDisplaySelect').val('tomorrow')
        else $('#taskDisplaySelect').val('fixed')
    }

    getTasks(true)
}

function updateIntervalText() {
    let word
    switch($('#dueSelect').val()) {
        case 'daily':
            word = 'day'
            break
        case 'monthly':
            word = 'month'
            break
        case 'yearly':
            word = 'year'
            break
    }
    if($('#intervalInput').val() == 1) $('#intervalText').text(word)
    else $('#intervalText').text(word + 's')
}

function hideSecondDate() {
    $('#secondDateSelector').fadeOut(400, () => $('#secondDateSelectorInput').val(''))
    $('#dateSelectorDash').fadeOut(400)
}

function showSecondDate() {
    $('#secondDateSelector').fadeIn(400)
    $('#dateSelectorDash').fadeIn(400)
}

function toggleLink(link) {
    $('.nav-pills .nav-link.active').removeClass('active')
    $(link).addClass('active')
    getTasks(true, true, true)
}

function today() {
    const actualNow = DateTime.now()
    if(!areEqual(now, actualNow)) {
        now = actualNow
        sync()
    }
    return now
}

function todayPlusDays(toAdd) {
    return today().plus({ days: toAdd })
}

function isYesterday(date) {
    return areEqual(date, todayPlusDays(-1))
}

function isToday(date) {
    return areEqual(date, today())
}

function isTomorrow(date) {
    return areEqual(date, todayPlusDays(1))
}

function areEqual(date, date2) {
    return date.toISODate() == date2.toISODate()
}

function playSound(location) {
    var sound = new Audio(location)
    sound.loop = false
    sound.play()
}

// https://stackoverflow.com/a/35970186/5905216
function invertColor(hex, bw) {
    if (hex.indexOf('#') === 0) {
        hex = hex.slice(1);
    }
    // convert 3-digit hex to 6-digits.
    if (hex.length === 3) {
        hex = hex[0] + hex[0] + hex[1] + hex[1] + hex[2] + hex[2];
    }
    if (hex.length !== 6) {
        throw new Error('Invalid HEX color.');
    }
    var r = parseInt(hex.slice(0, 2), 16),
        g = parseInt(hex.slice(2, 4), 16),
        b = parseInt(hex.slice(4, 6), 16);
    if (bw) {
        // https://stackoverflow.com/a/3943023/112731
        return (r * 0.299 + g * 0.587 + b * 0.114) > 186
            ? '#000000'
            : '#FFFFFF';
    }
    // invert color components
    r = (255 - r).toString(16);
    g = (255 - g).toString(16);
    b = (255 - b).toString(16);
    // pad each with zeros and return
    return "#" + padZero(r) + padZero(g) + padZero(b);
}
