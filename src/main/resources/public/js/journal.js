const searchParams = new URLSearchParams(window.location.search)
const journalID = searchParams.get('id')

$(document).ready(function() {
    $('#newEntryBtn').click(function() {
        $.post(`/api/v1/journal/${journalID}/edit/newEntry`, entryID => {
            window.location.replace(`/journal/edit?journal=${journalID}&entry=${entryID}`)
        })
    })

    $.get(`/api/v1/journal/${journalID}/`, journal => {
        $('#journalTitle').html(journal.title)
        $('#journalInfo').html(journal.entryCount + (journal.entryCount == 1 ? ' entry' : ' entries') + ' · Last updated ' + journal.lastUpdatedText)
    })

    if(searchParams.get('q')) {
        const query = searchParams.get('q')
        $('#journalSearchField').val(query)
        $('#resultCount').show()
        $.get(`/api/v1/journal/${journalID}/entries?q=${query}`, displayEntries)
    } else {
        $.get('/api/v1/journal/' + journalID + '/entries', displayEntries)
    }

    $('#randomEntryBtn').click(function() {
        $.get(`/api/v1/journal/${journalID}/randomEntry`, entry => {
            window.location.replace(`/journal/edit?journal=${journalID}&entry=${entry}`)
        })
    })

    $('#exportJournalBtn').click(() => {
        window.location = `/api/v1/journal/${journalID}/export`
    })

    $('#journalSearchField').on('keydown', e => {
        if(e.key == 'Enter') $('#journalSearchBtn').click()
    })

    $('#searchClearLink').click(() => {
        searchParams.delete('q')
        window.history.pushState(null, null, '?' + searchParams.toString())
        window.location.reload()
    })

    $('#journalSearchBtn').click(() => {
        const query = $('#journalSearchField').val()
        if(query.trim().length == 0) return
        searchParams.set('q', query)
        window.history.pushState(null, null, '?' + searchParams.toString())
        $('#resultCount').show()
        $.get(`/api/v1/journal/${journalID}/entries?q=${query}`, displayEntries)
    })
})

function displayEntries(entries) {
    if(entries.length == 0) {
        $('#resultCountText').hide()
        $('#randomEntryBtn').hide()
        $('#entryList').html('<span style="margin-left: 32px">No entries found. <a href="#" id="newEntryLink">Create a new one</a></span>')
        $('#newEntryLink').click(() => $('#newEntryBtn').click())
        return
    } else {
        $('#resultCountText').show()
        $('#resultCountText').text(`${entries.length} ${entries.length == 1 ? 'entry' : 'entries'} found`)
    }
    $('#entryList').html('')
    entries.forEach(function(entry) {
        var newItem = $(`
                        <a class="journal-link" href="/journal/edit?journal=${journalID}&entry=${entry.id}">
                          <li class="list-group-item">
                            ${entry.title.trim().length == 0 ? "(no title)" : entry.title}<br>
                            <small>${entry.dateText}<br>${removeTags(entry.contentPreview)}</small>
                          </li>
                        </a>
                        `)
        $('#entryList').append(newItem)
    })
}

// https://www.tutorialspoint.com/how-to-remove-html-tags-from-a-string-in-javascript
function removeTags(str) {
  if(str === null || str === '') return '(no content)';
  else str = str.toString();
  return str.replace(/(<([^>]+)>)/ig, '');
}
