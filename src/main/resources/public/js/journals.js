$(document).ready(function() {
    $('#actualCreateJournalBtn').click(() => {
        var title = $('#newJournalTitleField').val()
        console.log(title)
        if(title.length == 0 || title.trim() === '') return
        $('#newJournalTitleField').val('')
        console.log('got here')
        $.ajax({
            url: '/api/v1/newJournal',
            type: 'POST',
            data: title,
            contentType: 'application/json'
        }).done(function() {
            $('#journalList').html('')
            getJournals()
        })
    })

    $('#actualImportJournalBtn').click(() => {
        //$(this).html('<span class="spinner-border spinner-border-sm" role="status" aria-hidden="true"></span> Importing...')
        //$(this).prop('disabled', true)
        const formData = new FormData()
        formData.append('file', $('#journalFileField')[0].files[0])
        $.ajax({
            url: '/api/v1/importJournal',
            type: 'POST',
            contentType: false,
            processData: false,
            data: formData,
            success: journalID => {
                //$(this).text('Import')
                //$(this).prop('disabled', false)
                alert('Import successful!')
                window.location = `/journal?id=${journalID}`
            },
            error: () => {
                //$(this).text('Import')
                //$(this).prop('disabled', false)
                alert('An error occurred.')
            }
        })
    })

    getJournals()
})

function getJournals() {
    $.get('/api/v1/journals', journals => {
        $('#journalCount').html(journals.length + (journals.length == 1 ? ' journal' : ' journals'))
        journals.forEach(function(journal) {
            var newItem = $(`
                            <a class="journal-link" href="/journal?id=${journal.id}">
                              <div id="${journal.id}" class="card">
                                <h1>${journal.title}</h1>
                                <small>${journal.entryCount} ${journal.entryCount == 1 ? "entry" : "entries"} · Last updated ${journal.lastUpdatedText}</small>
                              </div>
                            </a>
                            `)
            $('#journalList').append(newItem)
        })
        $('#journalList').append(`
                                 <a class="journal-link" href="#" data-bs-toggle="modal" data-bs-target="#journalCreateModal">
                                  <div class="card">
                                    <h3 style="text-align: center">
                                      <i class="bi bi-plus-circle"></i>
                                      <p style="margin-top: 5px">Create Journal</p>
                                    </h3>
                                  </div>
                                </a>
                                `)
    })
}