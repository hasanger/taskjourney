var searchParams = new URLSearchParams(window.location.search)
var journalID = searchParams.get('journal')
var entryStarted, entryFinished
var saved = true
var initialContent

tinymce.init({
    selector: '#writingArea',
    menubar: false,
    statusbar: false,
    nowrap: false,
    skin: 'dark',
    content_css: 'dark',
    forced_root_block: 'div',
    save_onsavecallback: save,
    save_enablewhendirty: false,
    plugins: 'save searchreplace lists image link',
    toolbar: 'save | fontfamily fontsize | bold italic underline strikethrough | bullist numlist | image link hr | superscript subscript | backcolor forecolor | indent outdent | alignleft aligncenter alignright alignjustify | removeformat',
    font_family_formats: 'Open Sans=open sans,sans-serif; Andale Mono=andale mono,monospace; Arial=arial,helvetica,sans-serif; Arial Black=arial black,avant garde; Book Antiqua=book antiqua,palatino; Comic Sans MS=comic sans ms,sans-serif; Courier New=courier new,courier; Georgia=georgia,palatino; Helvetica=helvetica; Impact=impact,chicago; Symbol=symbol; Tahoma=tahoma,arial,helvetica,sans-serif; Terminal=terminal,monaco; Times New Roman=times new roman,times; Trebuchet MS=trebuchet ms,geneva; Verdana=verdana,geneva; Webdings=webdings; Wingdings=wingdings,zapf dingbats',
    content_style: '.mce-content-body > *:last-child{padding-bottom: 16px}',
    setup: function(editor) {
        editor.on('change input', function() {
            saved = false
            $('button[title="Save"] path').attr('fill', '#dc3545')
        })
        editor.addShortcut('ctrl+o', '', function() {})
    },
    init_instance_callback: initEditor
})

function initEditor(editor) {
    waitFor(_ => initialContent != undefined)
    .then(_ => {
        editor.setContent(initialContent)
        editor.undoManager.clear()
    })
}

function waitFor(conditionFunction) {

  const poll = resolve => {
    if(conditionFunction()) resolve();
    else setTimeout(_ => poll(resolve), 100);
  }

  return new Promise(poll);
}

$(document).ready(function() {
    $('#writingCol').css('visibility', 'hidden')
    setTimeout(function() {
        $.get(`/api/v1/journal/${searchParams.get('journal')}/entry/${searchParams.get('entry')}`, entry => {
            $('.tox-editor-container').prepend($(`
                                                 <div>
                                                   <input id="entryTitle" placeholder="Entry Title" autocomplete="off">
                                                   <div id="entryOptions">
                                                     <div id="entryDateContainer">
                                                       <i class="bi bi-calendar2"></i>
                                                       <input id="entryDate" type="date">
                                                     </div>
                                                     <button id="stopwatchBtn" class="btn"><i class="bi bi-stopwatch"></i></button>
                                                     <div id="moreOptionsDropdown" class="dropdown">
                                                        <button id="moreOptionsBtn" class="btn dropdown-toggle" data-bs-toggle="dropdown"><i class="bi bi-three-dots-vertical"></i></button>
                                                        <ul class="dropdown-menu" aria-labelledby="moreOptionsBtn">
                                                          <li><a id="moreInfoBtn" class="dropdown-item" data-bs-toggle="modal" data-bs-target="#moreInfoModal"><i class="bi bi-info-circle"></i> More info</a></li>
                                                          <li><a id="deleteEntryBtn" class="dropdown-item"><i class="bi bi-trash"></i> Delete entry</a></li>
                                                        </ul>
                                                     </div>
                                                   </div>
                                                 </div>
                                                 `))
            $('.tox-editor-container').append($('<div id="statusBar">Started: - | Finished: -</div>'))

            $('#entryDateContainer').click(function() {
                document.querySelector('#entryDate').showPicker()
            })

            const stopwatch = {
                startTime: null,
                finishTime: null,
                running: false,
                time: 0,
                fullTime: '00:00:00.00',
                start: function() {
                    stopwatch.running = true
                    stopwatch.time = 0
                    stopwatch.startTime = new Date()
                    stopwatch.finishTime = null
                    entryStarted = stopwatch.startTime
                    entryFinished = undefined
                    $('#startStopBtn').html('<i class="bi-stop-fill"></i>')
                    unsaved()
                    updateStatusBar()
                },
                stop: function() {
                    stopwatch.running = false
                    stopwatch.finishTime = new Date()
                    entryStarted = stopwatch.startTime
                    entryFinished = stopwatch.finishTime
                    $('#stopwatch').text(stopwatch.fullTime)
                    $('#startStopBtn').html('<i class="bi-play-fill"></i>')
                    unsaved()
                    updateStatusBar()
                },
                reset: function() {
                    stopwatch.running = false
                    stopwatch.startTime = null
                    stopwatch.finishTime = null
                    entryStarted = undefined
                    entryFinished = undefined
                    stopwatch.time = 0
                    stopwatch.fullTime = '00:00:00.00'
                    $('#stopwatch').text(stopwatch.fullTime)
                    $('#startStopBtn').html('<i class="bi-play-fill"></i>')
                    unsaved()
                    updateStatusBar()
                },
                tick: function() {
                    stopwatch.time++
                    var hours = 0, minutes = 0, seconds = 0, centiseconds = 0, remaining = stopwatch.time
                    hours = Math.floor(remaining / 360000)
                    remaining -= hours * 360000
                    minutes = Math.floor(remaining / 6000)
                    remaining -= minutes * 6000
                    seconds = Math.floor(remaining / 100)
                    remaining -= seconds * 100
                    centiseconds = remaining

                    if(hours < 10) hours = '0' + hours
                    if(minutes < 10) minutes = '0' + minutes
                    if(seconds < 10) seconds = '0' + seconds
                    if(centiseconds < 10) centiseconds = '0' + centiseconds

                    stopwatch.fullTime = hours + ':' + minutes + ':' + seconds + '.' + centiseconds
                    $('#stopwatch').text(stopwatch.fullTime)
                }
            }

            setInterval(function() {
                if(stopwatch.running) stopwatch.tick()
            }, 10)

            const stopwatchTooltip = new bootstrap.Tooltip(document.querySelector('#stopwatchBtn'), {
                title: `
                       <p id="stopwatch">${stopwatch.fullTime}</p>
                       <div id="startStopBtn" class="btn"><i class="bi-play-fill"></i></div>
                       <div id="resetBtn" class="btn"><i class="bi-trash3-fill"></i></div>
                       `,
                html: true,
                placement: 'right',
                trigger: 'manual'
            })
            stopwatchTooltip.show()
            $('.tooltip').hide()

            $('#stopwatchBtn').click(function() {
                if($('.tooltip').is(':visible')) $('.tooltip').hide()
                else $('.tooltip').show()
            })

            $(document).on('click', '#startStopBtn', function() {
                if(stopwatch.running) stopwatch.stop()
                else stopwatch.start()
            })

            $(document).on('click', '#resetBtn', function() {
                stopwatch.reset()
            })

            $('#entryTitle').val(entry.title)
            initialContent = entry.content
            document.querySelector('#entryDate').valueAsDate = new Date(entry.date)

            $('#timeCreatedText').text(new Date(entry.created).toLocaleString())
            $('#lastUpdatedText').text(new Date(entry.lastUpdated).toLocaleString())

            if(entry.started != undefined) entryStarted = new Date(entry.started)
            if(entry.finished != undefined) entryFinished = new Date(entry.finished)
            if(entryStarted != undefined && entryFinished != undefined) {
                stopwatch.time = Math.round(((entryFinished.valueOf() - entryStarted.valueOf()) / 10) - 1)
                stopwatch.tick()
            }
            updateStatusBar()

            $('#deleteEntryBtn').click(function() {
                var confirmation = confirm('Are you sure you want to delete the entry? This cannot be undone.')
                if(confirmation == true) {
                    $.ajax({
                        url: `/api/v1/journal/${searchParams.get('journal')}/edit/entry/${searchParams.get('entry')}/delete`,
                        type: 'DELETE'
                    })
                    window.location.replace(`/journal?id=${journalID}`)
                }
            })
            $('#writingCol').hide()
            $('#writingCol').css('visibility', 'inherit')
            $('#writingCol').fadeIn(500)

            $('#entryTitle, #entryDate').on('input', unsaved)
        })
    }, 200)

    
    $('#journalSearchField').on('keydown', e => {
        if(e.key == 'Enter') $('#journalSearchBtn').click()
    })

    $('#journalSearchBtn').click(() => {
        window.location = `/journal?id=${journalID}&q=${$('#journalSearchField').val()}`
    })

    $('#newEntryBtnWide').click(() => {
        $.post(`/api/v1/journal/${journalID}/edit/newEntry`, entryID => {
            window.location.replace(`/journal/edit?journal=${journalID}&entry=${entryID}`)
        })
    })

    $('#journalTitle, #allEntriesBtn').attr('href', '/journal?id=' + journalID)

    $('#randomEntryBtn').click(() => {
        $.get(`/api/v1/journal/${journalID}/randomEntry`, entryID => {
            window.location.replace(`/journal/edit?journal=${journalID}&entry=${entryID}`)
        })
    })

    $.get(`/api/v1/journal/${journalID}/`, function(journal) {
        $('#journalTitle').html(journal.title)
        $('#journalInfo').html(journal.entryCount + (journal.entryCount == 1 ? ' entry' : ' entries') + ' ● Last updated ' + journal.lastUpdatedText)
    })

    getEntries()

    $(window).on('beforeunload', function(e) {
        if(!saved) {
            e.preventDefault()
            e.returnValue = ''
            return ''
        }
    })

    $('body').on('keydown', function(e) {
        if(e.ctrlKey && e.key === 's') {
            e.preventDefault()
            $('button[title="Save"]').click()
        }
    })
})

function unsaved() {
    $('button[title="Save"] path').attr('fill', '#dc3545')
    saved = false
}

function save() {
    $.ajax({
        url: `/api/v1/journal/${searchParams.get('journal')}/edit/entry/${searchParams.get('entry')}`,
        type: 'PATCH',
        data: JSON.stringify({
            title: $('#entryTitle').val(),
            content: tinymce.activeEditor.getContent(),
            date: $('#entryDate').val(),
            started: entryStarted,
            finished: entryFinished
        }),
        success: () => {
            $('button[title="Save"] path').removeAttr('fill')
            saved = true
            $('#entryList').html('')
            getEntries()
        }
    })
}

function getEntries() {
    $.get(`/api/v1/journal/${journalID}/entries?offset=${searchParams.get('entry')}&count=11`, entries => {
        entries.forEach(entry => {
            var newItem = $(`
                            <a class="journal-link" href="/journal/edit?journal=${journalID}&entry=${entry.id}">
                              <li class="small-entry-item list-group-item${entry.id == searchParams.get('entry') ? ' entry-highlighted' : ''}">
                                ${entry.title.trim().length == 0 ? "(no title)" : entry.title}<br>
                                <small>${entry.dateText}</small>
                              </li>
                            </a>
                            `)
            $('#entryList').append(newItem)
        })
    })
}

function updateStatusBar() {
    if(entryStarted == undefined && entryFinished == undefined) $('#statusBar').hide()
    else $('#statusBar').show()
    $('#statusBar').html(`Started: ${entryStarted == undefined ? '-' : entryStarted.toLocaleString()} <div class="vr"></div> Finished: ${entryFinished == undefined ? '-' : entryFinished.toLocaleString()}`)
}
