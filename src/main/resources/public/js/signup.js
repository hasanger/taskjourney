$(document).ready(function() {
	$('body').keypress(function(e) {
		if(e.key == 'Enter') {
			e.preventDefault()
			$('#signUpBtn').click()
		}
	})
	
	$('#signUpBtn').click(function() {
		$('.form-control').removeClass('is-invalid')

		var data = {
			email: $('#emailField').val(),
			username: $('#usernameField').val(),
			password: $('#passwordField').val(),
			rememberMe: $('#rememberMeCheckbox').prop('checked')
		}

		if(isEmpty($('#emailField').val())) {
			$('#emailField').addClass('is-invalid')
			$('#emailField + .invalid-feedback').text('Please enter an email address.')
			return
		}

		if(!(/^[^\s@]+@[^\s@]+\.[^\s@]+$/.test($('#emailField').val()))) {
			$('#emailField').addClass('is-invalid')
			$('#emailField + .invalid-feedback').text('Please enter a valid email address.')
			return
		}

		if(isEmpty($('#usernameField').val())) {
			$('#usernameField').addClass('is-invalid')
			$('#usernameField + .invalid-feedback').text('Please enter a username.')
			return
		}

		if(isEmpty($('#passwordField').val())) {
			$('#passwordField').addClass('is-invalid')
			$('#passwordField + .invalid-feedback').text('Please enter a password.')
			return
		}

		if(!($('#passwordField').val() == $('#confirmPasswordField').val())) {
			$('#confirmPasswordField').addClass('is-invalid')
			return
		}

		if($.trim($('#firstNameField').val()).length) data.firstName = $('#firstNameField').val()
		if($.trim($('#lastNameField').val()).length) data.lastName = $('#lastNameField').val()

		$.ajax({
			url: '/api/v1/signup',
			method: 'POST',
			data: JSON.stringify(data),
			success: function(data) {
				window.location = '/'
			},
			error: function(jqXHR, textStatus, errorThrown) {
				var data = JSON.parse(jqXHR.responseText)
				switch(data.reason) {
					case 'Already signed in':
						window.location = '/'
						break

					case 'Email already taken':
						$('#emailField').addClass('is-invalid')
						$('#emailField + .invalid-feedback').text('Email is taken.')
						break

					case 'Username already taken':
						$('#usernameField').addClass('is-invalid')
						$('#usernameField + .invalid-feedback').text('Username is taken.')
						break
				}
			}
		})
	})

	$('#signInLogo').click(function() {
		window.location = '/'
	})
})

function isEmpty(string) {
	return !$.trim(string).length
}

// https://stackoverflow.com/a/11268104/5905216
function scorePassword(pass) {
    var score = 0;
    if (!pass)
        return score;

    // award every unique letter until 5 repetitions
    var letters = new Object();
    for (var i=0; i<pass.length; i++) {
        letters[pass[i]] = (letters[pass[i]] || 0) + 1;
        score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    var variations = {
        digits: /\d/.test(pass),
        lower: /[a-z]/.test(pass),
        upper: /[A-Z]/.test(pass),
        nonWords: /\W/.test(pass),
    }

    var variationCount = 0;
    for (var check in variations) {
        variationCount += (variations[check] == true) ? 1 : 0;
    }
    score += (variationCount - 1) * 10;

    return parseInt(score);
}

function checkPassStrength(pass) {
    var score = scorePassword(pass);
    if (score > 80)
        return "strong";
    if (score > 60)
        return "good";
    if (score >= 30 || pass.length <= 8)
        return "weak";

    return "";
}
